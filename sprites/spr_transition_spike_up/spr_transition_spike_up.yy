{
    "id": "8baca9f9-ffc1-45be-9e94-986068a59815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_spike_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60e6cded-a9a1-49d1-a0a9-89409329d805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8baca9f9-ffc1-45be-9e94-986068a59815",
            "compositeImage": {
                "id": "bca34834-7f7f-40b8-9649-2835b294f296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60e6cded-a9a1-49d1-a0a9-89409329d805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0df81537-a078-4832-a4b1-6f6882a78262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60e6cded-a9a1-49d1-a0a9-89409329d805",
                    "LayerId": "4c382927-8687-419b-b9cd-265d9d6a1109"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4c382927-8687-419b-b9cd-265d9d6a1109",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8baca9f9-ffc1-45be-9e94-986068a59815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}