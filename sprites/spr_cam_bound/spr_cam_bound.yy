{
    "id": "452b0a1c-597f-408a-9cce-31603ec7f72e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cam_bound",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20a36092-0718-4e71-827d-343881c41079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452b0a1c-597f-408a-9cce-31603ec7f72e",
            "compositeImage": {
                "id": "909e1427-cb5a-45a3-9f2f-0be4f7398155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20a36092-0718-4e71-827d-343881c41079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd16b0a2-dc32-451a-afd6-2c394569cca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20a36092-0718-4e71-827d-343881c41079",
                    "LayerId": "09ea92d2-0f55-4531-a6cf-604dce00638e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "09ea92d2-0f55-4531-a6cf-604dce00638e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "452b0a1c-597f-408a-9cce-31603ec7f72e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}