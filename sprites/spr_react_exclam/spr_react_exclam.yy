{
    "id": "3f09882c-7ebb-40da-af69-4602c9018669",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_react_exclam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a1e41dd-df7e-4962-9268-0d7fba7b8fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f09882c-7ebb-40da-af69-4602c9018669",
            "compositeImage": {
                "id": "494a942d-1c33-4e82-9edb-64636dbc6de5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1e41dd-df7e-4962-9268-0d7fba7b8fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683b360d-bf1d-4535-8707-4ae725eafacd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1e41dd-df7e-4962-9268-0d7fba7b8fb4",
                    "LayerId": "31543e24-e1af-410f-b326-2eb82b004662"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "31543e24-e1af-410f-b326-2eb82b004662",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f09882c-7ebb-40da-af69-4602c9018669",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}