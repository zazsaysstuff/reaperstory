{
    "id": "8d2654c0-177d-488b-ac55-d51c78e33f70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_pause_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c445667b-9c29-46e5-a038-ff058af5e198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d2654c0-177d-488b-ac55-d51c78e33f70",
            "compositeImage": {
                "id": "d4ec6679-0376-4daf-9765-51ea04beb834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c445667b-9c29-46e5-a038-ff058af5e198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b8515e-097d-415f-8acc-53b8614a86cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c445667b-9c29-46e5-a038-ff058af5e198",
                    "LayerId": "2e3b7c05-5f0c-4b84-8aeb-b4a647901837"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2e3b7c05-5f0c-4b84-8aeb-b4a647901837",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d2654c0-177d-488b-ac55-d51c78e33f70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 0
}