{
    "id": "87dffb71-24dd-441d-813f-815aab8b8458",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reaper_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 26,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5b2eaaf-d1b4-4a74-b452-27bea306678e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dffb71-24dd-441d-813f-815aab8b8458",
            "compositeImage": {
                "id": "80499d32-a40b-42c6-b9c1-0754f5796a53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5b2eaaf-d1b4-4a74-b452-27bea306678e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b413ef16-969d-4c66-ad7f-13157d985291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5b2eaaf-d1b4-4a74-b452-27bea306678e",
                    "LayerId": "60eb4094-4219-4538-b463-a6e7044b4c4b"
                }
            ]
        },
        {
            "id": "87baeb34-1ee1-46e8-a273-26956ea3ede3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dffb71-24dd-441d-813f-815aab8b8458",
            "compositeImage": {
                "id": "4baef770-c19b-4a9a-815a-4fcaefec43ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87baeb34-1ee1-46e8-a273-26956ea3ede3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d748567-0e70-4290-8410-194deda561a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87baeb34-1ee1-46e8-a273-26956ea3ede3",
                    "LayerId": "60eb4094-4219-4538-b463-a6e7044b4c4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "60eb4094-4219-4538-b463-a6e7044b4c4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87dffb71-24dd-441d-813f-815aab8b8458",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 18,
    "yorig": 40
}