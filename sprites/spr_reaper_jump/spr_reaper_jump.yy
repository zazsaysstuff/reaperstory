{
    "id": "fbe48d56-c255-4712-9729-ff87cd968348",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reaper_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 26,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6859a0e-a180-4056-86ed-e871d9a8f8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe48d56-c255-4712-9729-ff87cd968348",
            "compositeImage": {
                "id": "5d0a2690-6729-4add-90a9-58cf8548f73e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6859a0e-a180-4056-86ed-e871d9a8f8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38ab314-cb5c-4497-84ab-272f4707fdc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6859a0e-a180-4056-86ed-e871d9a8f8e4",
                    "LayerId": "278a925a-549f-4a78-935c-225a593e048c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "278a925a-549f-4a78-935c-225a593e048c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbe48d56-c255-4712-9729-ff87cd968348",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 18,
    "yorig": 40
}