{
    "id": "4dc34f61-228d-43d8-8bc5-00b08367a116",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_spike_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "598bffd4-eeeb-4440-8639-b41c0e657c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dc34f61-228d-43d8-8bc5-00b08367a116",
            "compositeImage": {
                "id": "503c53e5-d6de-4c4f-9d3b-a6317e616faa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598bffd4-eeeb-4440-8639-b41c0e657c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c30badc-2a1c-48ce-8b88-a59607e4065b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598bffd4-eeeb-4440-8639-b41c0e657c18",
                    "LayerId": "ad09e047-3285-40c2-b97e-15c89abf32ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad09e047-3285-40c2-b97e-15c89abf32ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dc34f61-228d-43d8-8bc5-00b08367a116",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}