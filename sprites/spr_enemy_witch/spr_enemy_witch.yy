{
    "id": "e140fd40-38a1-4793-b986-fd4aed0563e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_witch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 138,
    "bbox_left": 11,
    "bbox_right": 163,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e74156c-4ee3-4187-884a-07c79fec3b8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e140fd40-38a1-4793-b986-fd4aed0563e2",
            "compositeImage": {
                "id": "4c59eba8-be09-46bf-b84f-937d687c21a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e74156c-4ee3-4187-884a-07c79fec3b8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8d75e5-add9-4e07-a6e5-54e62e9402de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e74156c-4ee3-4187-884a-07c79fec3b8f",
                    "LayerId": "6a118c29-29b3-4950-b78c-7c4bd4853c17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 147,
    "layers": [
        {
            "id": "6a118c29-29b3-4950-b78c-7c4bd4853c17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e140fd40-38a1-4793-b986-fd4aed0563e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 179,
    "xorig": 89,
    "yorig": 73
}