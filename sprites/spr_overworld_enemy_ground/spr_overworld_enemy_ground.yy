{
    "id": "150ee6a6-2a4c-4f3e-9277-78cbf4f9c9ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld_enemy_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cfdeda6-b789-41f3-ba55-db8ec6fdd9a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "150ee6a6-2a4c-4f3e-9277-78cbf4f9c9ad",
            "compositeImage": {
                "id": "ac7de472-c192-461c-be11-ef1d147ce90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cfdeda6-b789-41f3-ba55-db8ec6fdd9a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed52dcbb-7f66-446d-bc92-4ea614f10bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cfdeda6-b789-41f3-ba55-db8ec6fdd9a7",
                    "LayerId": "ccd317f5-e195-45e0-8625-760718c8fcbe"
                }
            ]
        },
        {
            "id": "974c7e48-22ff-4d4b-bc37-d0fdeddfcc70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "150ee6a6-2a4c-4f3e-9277-78cbf4f9c9ad",
            "compositeImage": {
                "id": "01acab84-1cc3-4ce8-b8df-a80da0c0ad91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974c7e48-22ff-4d4b-bc37-d0fdeddfcc70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1242a03a-bc87-4d8a-97fe-8b32c9c4fc95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974c7e48-22ff-4d4b-bc37-d0fdeddfcc70",
                    "LayerId": "ccd317f5-e195-45e0-8625-760718c8fcbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "ccd317f5-e195-45e0-8625-760718c8fcbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "150ee6a6-2a4c-4f3e-9277-78cbf4f9c9ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 20
}