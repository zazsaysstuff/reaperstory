{
    "id": "3658a46b-e2fa-4324-89aa-6727dadd484e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tl_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 32,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bdfe476-3fac-45bd-b464-87f5cdafc12b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3658a46b-e2fa-4324-89aa-6727dadd484e",
            "compositeImage": {
                "id": "b94b5274-3c78-43f8-89c2-f1245db12ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bdfe476-3fac-45bd-b464-87f5cdafc12b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "304e8077-e2bd-4ed9-bbd2-013f7211b9de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bdfe476-3fac-45bd-b464-87f5cdafc12b",
                    "LayerId": "4b181a7d-7976-41e1-978a-0fd84cd568ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4b181a7d-7976-41e1-978a-0fd84cd568ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3658a46b-e2fa-4324-89aa-6727dadd484e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 0,
    "yorig": 0
}