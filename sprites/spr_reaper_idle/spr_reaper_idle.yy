{
    "id": "00d037b1-b71e-4701-b6ed-c7ccecc6f257",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reaper_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 26,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e73ce6f-4a6b-458a-85c6-a98284860554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00d037b1-b71e-4701-b6ed-c7ccecc6f257",
            "compositeImage": {
                "id": "b4e00f68-2c71-416f-bfb5-c1edd89f9dfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e73ce6f-4a6b-458a-85c6-a98284860554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b107f56-338f-4973-97b2-ae0b076bb33e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e73ce6f-4a6b-458a-85c6-a98284860554",
                    "LayerId": "2e1ecdfd-9017-459b-beac-f566cfd0a94a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2e1ecdfd-9017-459b-beac-f566cfd0a94a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00d037b1-b71e-4701-b6ed-c7ccecc6f257",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 18,
    "yorig": 40
}