{
    "id": "9686dcba-e128-4698-a112-f42eb36ba248",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_pause_icons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "382804e3-0680-47b1-825d-029939ca8663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9686dcba-e128-4698-a112-f42eb36ba248",
            "compositeImage": {
                "id": "abd37050-cef8-4ad8-b0d7-f18684294ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "382804e3-0680-47b1-825d-029939ca8663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4d3854c-2244-4801-9d94-6368c2135fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "382804e3-0680-47b1-825d-029939ca8663",
                    "LayerId": "2e10b3a8-1e2d-4637-9354-997fac00b344"
                }
            ]
        },
        {
            "id": "63715a50-f105-4299-8589-6b51911fe0d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9686dcba-e128-4698-a112-f42eb36ba248",
            "compositeImage": {
                "id": "067f0e06-e449-4903-87dd-dc823f2f024d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63715a50-f105-4299-8589-6b51911fe0d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed188f50-5683-4ab8-9f39-c40d035e0cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63715a50-f105-4299-8589-6b51911fe0d0",
                    "LayerId": "2e10b3a8-1e2d-4637-9354-997fac00b344"
                }
            ]
        },
        {
            "id": "c9f23a4a-79ca-46f6-b99c-e9a1f0bf3650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9686dcba-e128-4698-a112-f42eb36ba248",
            "compositeImage": {
                "id": "75f12470-7879-496f-a84b-cc7b827c5f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9f23a4a-79ca-46f6-b99c-e9a1f0bf3650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3d705b-0188-4b15-bbd9-5d72e44d04d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9f23a4a-79ca-46f6-b99c-e9a1f0bf3650",
                    "LayerId": "2e10b3a8-1e2d-4637-9354-997fac00b344"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "2e10b3a8-1e2d-4637-9354-997fac00b344",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9686dcba-e128-4698-a112-f42eb36ba248",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 36
}