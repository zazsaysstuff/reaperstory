{
    "id": "a9dfca76-e626-416d-b36e-2203235a19a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause_cursor",
    "eventList": [
        {
            "id": "95694060-e816-4a51-9a3c-884615e4718e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a9dfca76-e626-416d-b36e-2203235a19a6"
        },
        {
            "id": "bcc9d749-c280-47b5-96cf-bea7ca93ce65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a9dfca76-e626-416d-b36e-2203235a19a6"
        },
        {
            "id": "69de7584-d310-4ad4-9f91-5c0c02fc91b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a9dfca76-e626-416d-b36e-2203235a19a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d2654c0-177d-488b-ac55-d51c78e33f70",
    "visible": true
}