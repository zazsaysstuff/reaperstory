//Lower Bar
var max_height = 108;

bar_height = lerp(bar_height, max_height, 0.2);

//Text Alpha
if(bar_height > 100) pause_text_alpha = lerp(pause_text_alpha, 1, 0.1);

//Create Cursor
if(bar_height > 107.9 && cursor == noone)
{
	cursor = instance_create_depth(cursor_x, 0, depth + 1, obj_pause_cursor);
}

//Control Cursor
else if(cursor != noone)
{
	get_input();
	
	//Move Cursor
	if(left_pressed)
	{
		cursor_index = max(0, cursor_index - 1);
	}
	else if(right_pressed)
	{
		cursor_index = min(array_length_1d(option_icons) - 1, cursor_index + 1);
	}
	
	//Lerp to Position
	var icon = option_icons[cursor_index];
	
	cursor_x = icon.x;
	cursor_y = 120 + (sin(current_time/300) * 4);
	
	cursor.x = lerp(cursor.x, cursor_x, 0.2);
	cursor.y = lerp(cursor.y, cursor_y, 0.2);
	
	//Close Menu
	if(pause_pressed)
	{
		instance_destroy();
	}
}