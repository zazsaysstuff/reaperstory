{
    "id": "dfa73e4b-28ad-4980-b9cf-ea8e9083511f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause_menu",
    "eventList": [
        {
            "id": "c4d631d6-e4b4-4b1b-a7d4-6c52841a2011",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dfa73e4b-28ad-4980-b9cf-ea8e9083511f"
        },
        {
            "id": "5957c397-c0cc-489e-9b88-3ecb10f03abd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dfa73e4b-28ad-4980-b9cf-ea8e9083511f"
        },
        {
            "id": "9947a156-af6b-4e57-a007-1b4e6891b155",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dfa73e4b-28ad-4980-b9cf-ea8e9083511f"
        },
        {
            "id": "5f37589f-977f-448f-b191-cb885a07cc47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dfa73e4b-28ad-4980-b9cf-ea8e9083511f"
        },
        {
            "id": "3718aedd-4a05-40d8-8f5f-a36bc97faf50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "dfa73e4b-28ad-4980-b9cf-ea8e9083511f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}