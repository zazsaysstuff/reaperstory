//Pause Everything
global.time_scale = 0;

//Bar
bar_height = 0;

//Text
pause_text_alpha = -1;

//Options
options_list = [
	["Demons"],["Items"],["Options"]
];
option_icons = array_create(3);
cursor_index = 0;

//Create Buttons
for(var i = 0; i <= 2; i++)
{
	var xx = 48 + 102*i;
	var yy = 48;
	
	var b = instance_create_depth(xx, yy, depth - 1, obj_pause_icon);
	b.image_index = i;
	b.alarm[0] = 10 + 4*i;
	
	option_icons[i] = b;
}

//Cursor
cursor = noone;
cursor_x = option_icons[0].x;