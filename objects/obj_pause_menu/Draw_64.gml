//Draw Bar
var bar_color = $2C1C1A;
draw_set_color(bar_color);
draw_rectangle(0,0, window_get_width(), bar_height, false);

//Draw Text
draw_set_font(fntUI);
draw_set_halign(fa_right);

var op_sec = options_list[cursor_index];
draw_text_color(window_get_width() - 16, 36, op_sec[0], c_white, c_white, c_white, c_white, pause_text_alpha);

//Reset Color and Align
draw_set_halign(fa_left);
draw_set_color(c_white);