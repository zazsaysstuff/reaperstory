//Set Camera
#macro view view_camera[0]

camera_set_view_size(view, view_width, view_height);

if(follow != noone)
{
	var xx = clamp(follow.x - view_width/2, left_bound, right_bound - view_width);
	var yy = clamp(follow.y - view_height/2, top_bound, bottom_bound - view_height);
	
	camera_set_view_pos(view, xx, yy);
}

//Lerp Cameras
var spd = 0.1;
left_bound = lerp(left_bound, left_bound_set, spd);
right_bound = lerp(right_bound, right_bound_set, spd);
top_bound = lerp(top_bound, top_bound_set, spd);
bottom_bound = lerp(bottom_bound, bottom_bound_set, spd);