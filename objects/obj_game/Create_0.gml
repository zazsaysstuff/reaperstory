//Camera
view_width = 1920/3;
view_height = 1080/3;

window_scale = 2;

follow = noone;

left_bound = 0; right_bound = room_width;
top_bound = 0; bottom_bound = room_height;

left_bound_set = left_bound; right_bound_set = right_bound;
top_bound_set = top_bound; bottom_bound_set = bottom_bound;

window_set_size(view_width * window_scale, view_height * window_scale);
alarm[0] = 1;

surface_resize(application_surface, view_width * window_scale, view_height * window_scale);

//Globals
global.time_scale = 1;
global.active_screen = noone;

//Start Room
room_goto(rmTest001);