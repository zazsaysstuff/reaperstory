//Get Input
get_input();

//Mask
mask_index = spr_reaper_walk;

//Check if Grounded
grounded = place_meeting(x, y + 1, par_solid);

//Move States
switch(state)
{
	case reaper_state.normal: event_user(0); break;
}

//Lerp Face Scale
face_scale = lerp(face_scale, dir_facing, 0.5 * global.time_scale);

//Horizontal Collision
if(place_meeting(x + hspd, y, par_solid))
{
	while(!place_meeting(x + sign(hspd), y, par_solid))
	{
		x += sign(hspd);
	}
	hspd = 0;
}
x += hspd * global.time_scale;

//Gravity
if(!grounded)
{
	var grav_scale = 0.4;
	vspd += grav_scale * global.time_scale;
}

//Vertical Collision
if(place_meeting(x, y + vspd, par_solid))
{
	while(!place_meeting(x, y + sign(vspd), par_solid))
	{
		y += sign(vspd);
	}
	vspd = 0;
	
	//Squash
	if(place_meeting(x, y + 1, par_solid))
	{
		xscale = 1.6; yscale = 0.8;
	}
}
y += vspd * global.time_scale;

//Unsqaush n Stretch
xscale_spd = lerp(xscale_spd, (1 - xscale) * 0.25, 0.1);
yscale_spd = lerp(yscale_spd, (1 - yscale) * 0.25, 0.1);

xscale += xscale_spd * global.time_scale;
yscale += yscale_spd * global.time_scale;