/// @description Normal Movement

//Left and Right Walking
var spd_cap = 3;
if(left_held)
{
	//Set to Zero if Over 0
	if(hspd > 0) hspd = 0;
	
	//Lower Speed
	hspd = max(hspd - 1, -spd_cap);
	
	//Set Animation
	sprite_index = spr_reaper_walk;
	dir_facing = -1;
}
else if(right_held)
{
	//Set to Zero if under 0
	if(hspd < 0) hspd = 0;
	
	//Raise Speed
	hspd = min(hspd + 1, spd_cap);
	
	//Set Animation
	sprite_index = spr_reaper_walk;
	dir_facing = 1;
}

//Stop Walking
else
{
	sprite_index = spr_reaper_idle;
	hspd = 0;
}


//Jump
if(grounded && up_pressed) //TODO - Coyote Timers Maybe?
{
	var jump_height = 8;
	vspd = -jump_height;
	
	xscale = 0.8;
	yscale = 1.4;
}

if(!grounded) sprite_index = spr_reaper_jump;

//Activate Enemy Encounter
if(place_meeting(x, y, par_overworld_enemy))
{
	//Save Overworld Info
	
	//Start Transition
	state = reaper_state.paused;
	instance_create_depth(x, y - 32, depth + 1, obj_react_exclam);
}

//Open Pause Menu
if(pause_pressed && !instance_exists(obj_pause_menu))
{
	instance_create_depth(0, 0, depth, obj_pause_menu);
}