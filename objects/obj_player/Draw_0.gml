//Draw Self
image_speed = 1 * global.time_scale;

var xxscale = image_xscale * face_scale * xscale;
var yyscale = image_yscale * yscale;
draw_sprite_ext(sprite_index, image_index, x, y, xxscale, yyscale, image_angle, image_blend, image_alpha);