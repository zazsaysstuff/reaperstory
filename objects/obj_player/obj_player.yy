{
    "id": "9945d77d-5b2a-4323-8bf3-bbe89923e1eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "bc33659e-e084-4ae4-8b65-7ed69871d4d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9945d77d-5b2a-4323-8bf3-bbe89923e1eb"
        },
        {
            "id": "caf5249a-19a0-44fe-8858-59c5d2d364fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9945d77d-5b2a-4323-8bf3-bbe89923e1eb"
        },
        {
            "id": "08f6cde9-e50c-4a1e-99c0-0ac32fe779b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9945d77d-5b2a-4323-8bf3-bbe89923e1eb"
        },
        {
            "id": "1b8b6daa-074d-46d7-a161-0d21e44bbfe5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9945d77d-5b2a-4323-8bf3-bbe89923e1eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87dffb71-24dd-441d-813f-815aab8b8458",
    "visible": true
}