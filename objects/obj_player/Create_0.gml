//Movement
hspd = 0;
vspd = 0;

//Collision
grounded = false;

//Camera
obj_game.follow = self;

//State Machine
enum reaper_state
{
	normal, //Walkin' and Runnin'
	paused
}
state = reaper_state.normal; //Starting State

//Animation
xscale = 1; xscale_spd = 0;
yscale = 1; yscale_spd = 0;
dir_facing = 1;
face_scale = 1;