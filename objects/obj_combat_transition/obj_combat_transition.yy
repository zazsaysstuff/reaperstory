{
    "id": "dab93eb9-6f15-48a9-8d96-4862f948698b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_combat_transition",
    "eventList": [
        {
            "id": "8751d0b5-dc2b-47ed-bf88-d8e35192e626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dab93eb9-6f15-48a9-8d96-4862f948698b"
        },
        {
            "id": "d58f1668-eb58-4eda-a024-f2e42a66b99c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dab93eb9-6f15-48a9-8d96-4862f948698b"
        },
        {
            "id": "b3e26be2-dd3e-46a8-b0ff-4edbd15c3004",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dab93eb9-6f15-48a9-8d96-4862f948698b"
        },
        {
            "id": "72811653-0abc-479b-8e13-0695b285d5a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dab93eb9-6f15-48a9-8d96-4862f948698b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}