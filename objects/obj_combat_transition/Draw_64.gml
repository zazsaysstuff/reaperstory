//Draw Void
draw_set_color($3D2123);
draw_rectangle(0, 0, window_get_width(), bar_y, false);

//Draw Bar
var spike_count = 40;
for(var i = -3; i < spike_count; i++)
{
	var xx = scroll_amount + 32*i;
	var yy = bar_y;
	draw_sprite(spr_transition_spike_down, 0, xx, yy);
}