{
    "id": "56c25d98-d58a-4efb-88c6-3bb3c0f97904",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_react_exclam",
    "eventList": [
        {
            "id": "4b433b00-1855-4c71-b9e3-2f27a58635f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56c25d98-d58a-4efb-88c6-3bb3c0f97904"
        },
        {
            "id": "3d3e6866-2959-4a64-820f-36f196ea3ef2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56c25d98-d58a-4efb-88c6-3bb3c0f97904"
        },
        {
            "id": "64204766-c565-4435-9c65-15cf372ce87c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "56c25d98-d58a-4efb-88c6-3bb3c0f97904"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f09882c-7ebb-40da-af69-4602c9018669",
    "visible": true
}