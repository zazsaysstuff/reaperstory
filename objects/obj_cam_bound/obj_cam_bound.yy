{
    "id": "45e948eb-da65-4f78-a487-7b422f3ab8db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cam_bound",
    "eventList": [
        {
            "id": "fd9234db-01a9-4c35-ade3-098a24482be6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9945d77d-5b2a-4323-8bf3-bbe89923e1eb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "45e948eb-da65-4f78-a487-7b422f3ab8db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "452b0a1c-597f-408a-9cce-31603ec7f72e",
    "visible": false
}