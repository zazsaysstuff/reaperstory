if(instance_exists(obj_game))
{
	obj_game.left_bound_set = bbox_left;
	obj_game.right_bound_set = bbox_left + 640*image_xscale;
	
	obj_game.top_bound_set = bbox_top;
	obj_game.bottom_bound_set = bbox_top + 360*image_yscale;
	
	global.active_screen = self;
}