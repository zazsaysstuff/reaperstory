if(visible)
{
	//Lerp Scales
	image_xscale = lerp(image_xscale, 1, 0.2);
	image_yscale = lerp(image_yscale, 1, 0.2);

	//Funny Lerp Angle
	rot_speed = lerp(rot_speed, (0 - image_angle) * 0.2, 0.1);
	image_angle += rot_speed;
}