{
    "id": "40bb2223-bb1b-46e0-9519-2c7ed75ea580",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause_icon",
    "eventList": [
        {
            "id": "c27df744-d1df-42bd-826d-3109f86b40d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40bb2223-bb1b-46e0-9519-2c7ed75ea580"
        },
        {
            "id": "cd49d9dc-582b-47a8-9134-3d8b4737d65a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "40bb2223-bb1b-46e0-9519-2c7ed75ea580"
        },
        {
            "id": "fdcb739d-a3fb-495d-9c24-0e08cd719f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "40bb2223-bb1b-46e0-9519-2c7ed75ea580"
        },
        {
            "id": "c0666e0c-b7b0-429c-8012-e82a49425eb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "40bb2223-bb1b-46e0-9519-2c7ed75ea580"
        },
        {
            "id": "710e2a78-0328-4387-8c52-85a7a10893ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "40bb2223-bb1b-46e0-9519-2c7ed75ea580"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9686dcba-e128-4698-a112-f42eb36ba248",
    "visible": true
}