{
    "id": "d4a7068b-3332-44b4-a708-0cd14d4d9735",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_overworld_enemy",
    "eventList": [
        {
            "id": "7a1b0547-242a-40e7-bb81-c01788866348",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4a7068b-3332-44b4-a708-0cd14d4d9735"
        },
        {
            "id": "0f286764-a46a-4329-880d-e5454828c086",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d4a7068b-3332-44b4-a708-0cd14d4d9735"
        },
        {
            "id": "8b891d37-dd91-4670-9f60-9f86ea33dd46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d4a7068b-3332-44b4-a708-0cd14d4d9735"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "150ee6a6-2a4c-4f3e-9277-78cbf4f9c9ad",
    "visible": true
}