//Held
left_held = keyboard_check(vk_left);
right_held = keyboard_check(vk_right);
up_held = keyboard_check(vk_up);

//Pressed
left_pressed = keyboard_check_pressed(vk_left);
right_pressed = keyboard_check_pressed(vk_right);
up_pressed = keyboard_check_pressed(vk_up);
pause_pressed = keyboard_check_pressed(ord("X"));