{
    "id": "f1ee26fa-bde1-4d32-925f-62deda38f9e7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntUI",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ChevyRay - Slapface",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f503308d-c062-4195-abae-c9dd5b248daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 41,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "25532f40-8493-4020-9515-a86907204304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 281,
                "y": 88
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c425dfb4-aba4-4c8f-b398-fa6fa5b21868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 262,
                "y": 88
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cbe1d672-376f-4151-9b1a-c11cbd817a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 41,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 235,
                "y": 88
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cd1cce23-4c22-4cde-a0da-011caebaa30b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 213,
                "y": 88
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5ee036d9-02f8-4995-b63a-ba728e9a1830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 196,
                "y": 88
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a2afc2c8-56cc-46f5-9cbf-75f33df734da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 172,
                "y": 88
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "35040666-e92f-40e9-9f0e-4402c48379a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 41,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 163,
                "y": 88
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "45da9f77-f8a4-46aa-9d23-bb4fefdd5528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 151,
                "y": 88
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "18f4ee3d-cf2e-4435-908e-04193a038cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 88
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "28f51789-f3e3-4de8-99b0-b77c19236d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 293,
                "y": 88
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "78732630-68f6-499f-94d7-7e3a97a9b6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 117,
                "y": 88
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6df8c841-ca93-43a4-b80d-ba5a501ca6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 41,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 86,
                "y": 88
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7854fc34-ed98-480c-a853-fc974a70a7de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 67,
                "y": 88
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e0ef8ef9-048e-4e07-aaf5-c8813d60a725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 41,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 60,
                "y": 88
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2ca44a8a-22c7-42d7-b608-c3ae72e6667d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 43,
                "y": 88
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b0e4479c-5e75-4985-91b3-4db3dc024e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 21,
                "y": 88
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "330d13e2-b767-46e8-b4ab-dcdfea0d6a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 88
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0065d021-128c-4376-ab8d-f96e224d9914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 479,
                "y": 45
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e254ff9f-0411-4eb3-a9f6-6f51206af539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 457,
                "y": 45
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "121e5ca4-a816-4a7e-9e4b-d02e05e717db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 438,
                "y": 45
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7ee98d66-fcf0-4f80-8ab8-83a3fa3eeb85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 95,
                "y": 88
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d878442d-0fc7-4fa1-9103-b55f602e6734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 307,
                "y": 88
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ae6e41a8-7f0a-4464-b578-76a879209376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 329,
                "y": 88
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "65170c87-8452-4997-a445-1b8d4a1c2f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 351,
                "y": 88
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d350267c-fd6c-4faf-9552-6219663854c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 261,
                "y": 131
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ee288fcc-a367-46d1-92f4-6fd9175fc0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 41,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 254,
                "y": 131
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "51c81fbf-f6b8-4df9-aa08-414a72c93173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 41,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 247,
                "y": 131
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0013bf40-c301-41a3-bbcd-9a880c2b4a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 230,
                "y": 131
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "90381db3-7e8d-40c3-88e7-8c3c424469af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 211,
                "y": 131
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bf7159e2-1ba2-4ea6-8915-cfba86ea5849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 194,
                "y": 131
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6307c8dd-e812-4f11-8c87-c0beed7ba678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 172,
                "y": 131
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4662a526-ec42-4daf-bc10-ada16a48f01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 41,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 145,
                "y": 131
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "29213c05-5545-4dcd-a76b-e1e486bf292c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 123,
                "y": 131
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bc6be464-9378-44ac-9389-5f1c8a25d7c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 101,
                "y": 131
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "025828bd-9a7e-4fec-977f-2dce756b01ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 82,
                "y": 131
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a74e90e7-f61c-42d4-a76a-7d81e5568f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 60,
                "y": 131
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6e8156a7-df4b-49c4-848b-4984de4658ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 41,
                "y": 131
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b34acdae-6a8c-4ab1-988c-c730e397119a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 24,
                "y": 131
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0621e846-2424-4605-9223-31bde0c0cb96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 131
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "14d93acf-f48c-4c14-80d3-36b32b065d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 479,
                "y": 88
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7452347d-c749-45d6-80c0-ea5d4f89e568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 457,
                "y": 88
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b457d341-da4c-4d13-a393-848948ddc1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 438,
                "y": 88
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "643ed691-95df-431e-a73e-fe579a8bb152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 416,
                "y": 88
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9703b80b-2d5f-4aed-a36e-bdffda43687e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 397,
                "y": 88
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1f7041c2-1d5d-4811-908b-0b204df44aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 373,
                "y": 88
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bccd2fdd-b396-427f-b7e4-99e1f2f90c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 414,
                "y": 45
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cdc9937b-3bca-4fbe-b634-0ccfec7e9c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 392,
                "y": 45
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "19409d4b-23a6-4b4b-b31d-04bfe5510389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 370,
                "y": 45
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f702f278-14cb-47e8-91cb-f78d6b2c929a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2bbfd42b-467e-4701-bc70-86ff985c3cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "aa56ec35-3562-4077-9a9e-6d049aae051a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3124dcd7-73b5-43bc-b8f3-e2d77b6148b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 41,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a08c4cba-5bb2-4fc1-a017-08aae31cc0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e5bd67ee-c42b-4b73-9186-82bee82885a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "74060e7a-b736-4fc2-b7c7-c7e17a367418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 41,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f82cd080-23e9-4690-b00c-7803dfa4ccf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "50bb0f74-ae96-4423-9075-775cf27fe818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9883ff92-b3f7-4471-a32e-63d74e148fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 41,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "827d374b-a03f-41fc-89fa-120accca61a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f90f1a2c-2d9f-4285-88a5-970d31d9bf8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "94120d44-1ef2-45a2-b240-eb5d7990194a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "237cfa9e-db56-4da8-a33b-7015da726131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 41,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "21e7d11e-5953-4b6c-8e2e-8f6899e5106a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d8e62358-4164-4fd6-b71e-542e8d170dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "bb7ccee5-d432-4ba8-b822-560f72f6ecce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c05db906-6267-4b30-ac6f-51fe6496727d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d1fa052b-2cf7-4eeb-abb2-0b0024f97ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b90af2cc-a50e-4b4c-bb3d-72e4267ad193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f519e802-d1b0-4df4-bb4c-45535ff1bddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c5677050-dfd5-4536-a8c2-114963883dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "50de6037-5ec6-4718-8d58-e9e433fffb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7ed493ff-8c3d-472b-89d7-e95bb8c9e076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 144,
                "y": 45
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fe23bba3-20a5-48c6-9e73-1a4ae742a8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7ff693d8-4425-4f02-af5f-3708798271a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 339,
                "y": 45
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6935090d-7a7f-4cde-a0d3-999b27d8fdd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 320,
                "y": 45
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1ddcc243-b54f-4c45-bc2c-3a9d68a559a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 41,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 311,
                "y": 45
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e742d210-f98f-48d8-9c65-28a8d6f2dabf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 41,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 285,
                "y": 45
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f426af4a-76e3-4a21-87e7-7f589959a3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 263,
                "y": 45
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "056b6604-7b48-4c63-9478-7b5f70154762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 244,
                "y": 45
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6813fc45-e3b5-403b-a8fb-d264d56977ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 222,
                "y": 45
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "acf5ddfb-6770-4e38-88b3-a4be46c9067f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 41,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 200,
                "y": 45
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a55aaf36-6645-49bc-ad47-15d28b06ab4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 183,
                "y": 45
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0b34eaa1-3762-4b4e-9168-cf53f8d4731f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 353,
                "y": 45
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3fec081e-7b25-4fd3-b827-fd9dbd89ae3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 166,
                "y": 45
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "95ddd5b6-cc32-4606-8314-c546f63420b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 125,
                "y": 45
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "81064a3f-4338-4807-9135-94e6ef5d6af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 41,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 102,
                "y": 45
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "03f12c87-c3d6-499b-821d-e745927b1fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 41,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 76,
                "y": 45
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "da5e5f2c-da72-4414-a6ac-0a19b3f7dbf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 57,
                "y": 45
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "004b7ea7-c75f-43de-beca-9f28a8fe1fa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 38,
                "y": 45
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2b40cf5b-ecbc-492e-8d67-3423839f7e47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 19,
                "y": 45
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c2c306e8-d344-418f-bdbf-9bf928322c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 45
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "caf2b14f-8a6f-4478-a456-6916facb077b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 41,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 502,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e67437bc-93c8-4d25-a16c-7437a223b141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9023ed28-8618-4025-8e1a-31ec7e287029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 280,
                "y": 131
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "37a022e3-9b89-45c9-a4b1-6ddccd59d872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 41,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 302,
                "y": 131
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ea314b6b-387c-44ad-b638-b848df7158ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 84
        },
        {
            "id": "49c3f68b-5ee0-44d4-bafd-94b0986fcb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 65
        },
        {
            "id": "80c25d61-5249-48ec-a1fe-b4975e818da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 66
        },
        {
            "id": "f0c5d6f3-1cd1-456b-a012-3be04fdf2ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 67
        },
        {
            "id": "688b4020-80a5-4723-bc9f-92117d14947e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 68
        },
        {
            "id": "30d0e413-9906-4649-8572-e7fcc4be2106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 69
        },
        {
            "id": "f87fb3f3-5c1d-4b9d-979c-084bcbe82fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 70
        },
        {
            "id": "fcbe9206-1fc0-4fdd-b47a-6c2afe9b3ad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 71
        },
        {
            "id": "be5ea655-f11f-4379-96d4-b4d73e89a9e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 72
        },
        {
            "id": "447d327c-3c60-4ba3-a02b-7abbc1c8ba46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 73
        },
        {
            "id": "a9e4b716-e020-4e4a-9d3b-4b00a87f0105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 74
        },
        {
            "id": "2dcf328a-dbd7-47f3-857d-991efe04dc53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 75
        },
        {
            "id": "742b1ac3-b626-4c2c-b1a0-68885c62104d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 76
        },
        {
            "id": "1567e6f8-9224-4eb3-b504-152b83394e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 77
        },
        {
            "id": "5e1c2b53-409f-4b24-9514-0c8ceb4f02a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 78
        },
        {
            "id": "dc184a2c-9337-469d-978d-253a1b1bbe97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 79
        },
        {
            "id": "96037849-9458-41db-b1b1-a8d3a3d06f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 80
        },
        {
            "id": "f03efab0-fc0a-444c-982a-49c1e99b0b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 81
        },
        {
            "id": "8d6528fa-958a-46b3-ba15-33dd45fdc99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 82
        },
        {
            "id": "f63de652-1f0f-4aea-ae60-3fd23ea187f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 84
        },
        {
            "id": "8dcccdd7-943b-4541-aede-235cdd661ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 85
        },
        {
            "id": "86099c47-72c6-4ab3-82e9-7ac8e0aa1c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 86
        },
        {
            "id": "60d54b25-fcf9-46e6-b517-63f86304920b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "8e14fce3-511e-465e-9c11-8c716cca9d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 89
        },
        {
            "id": "dcd0bd8f-cb30-4b75-a115-2bc0dfe943fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 50
        },
        {
            "id": "d13837ec-13c7-40f6-91f0-086bdc53aa75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 52
        },
        {
            "id": "1a9c4a98-9c02-4c4a-99ed-2367b411bd0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 55
        },
        {
            "id": "0e9e2b4b-8db4-4b0b-9447-1f197fc09386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 57
        },
        {
            "id": "0427cc19-7c18-4c05-88da-65d85e4f71a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 52
        },
        {
            "id": "9a680181-6ac1-4495-a4d2-d45cd5e91214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 55
        },
        {
            "id": "180caee1-c2a9-426f-bfa5-f81f548f8927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 57
        },
        {
            "id": "12803f24-a9f4-4217-84eb-cf73ec5d10ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 50
        },
        {
            "id": "3d697e32-8b1d-4cf6-8f8f-9f42a82545a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 52
        },
        {
            "id": "63a0e963-4edb-4667-adf3-40f43dab0100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 55
        },
        {
            "id": "fb676be8-9c0d-4068-bbbd-82717286dc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 57
        },
        {
            "id": "f284b9fe-a857-4522-8f98-2215ac74b9a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 49
        },
        {
            "id": "ff368df0-e741-4976-980b-24d7fe7027b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 50
        },
        {
            "id": "9b4102a2-1bc2-4740-a64d-7daabbefb7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 52
        },
        {
            "id": "92f60992-491a-4130-877e-d1f4a912c18d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 55
        },
        {
            "id": "268e1363-2caa-4ff1-a080-430f61c4b749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 57
        },
        {
            "id": "05352572-2132-4ccd-8537-e2680a0fb9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 50
        },
        {
            "id": "17e16916-77d2-4f95-b1ab-e9f1bde060fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 52
        },
        {
            "id": "f55c02e4-e539-4464-bf7d-32791fa90067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 55
        },
        {
            "id": "cacd0d65-9cad-4550-b692-487ead85bec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 50
        },
        {
            "id": "c9aa96b1-b648-4b1a-94df-197c18d3a051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 52
        },
        {
            "id": "1f312169-5628-4931-9a12-28f3d99a989f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 55
        },
        {
            "id": "055b03cf-1fd1-48e8-bdf5-9f4d721fc6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 39
        },
        {
            "id": "1b11ad39-a546-4472-8a71-98189448212d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 68
        },
        {
            "id": "139c257c-274b-4fb8-b893-d4c84e43c67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 70
        },
        {
            "id": "1ef9b201-167a-4916-9503-a9039b45a5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 71
        },
        {
            "id": "f7ad13aa-928b-4a07-a8d7-26609196cf6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 72
        },
        {
            "id": "084bb434-d072-46e3-9399-e40298aef50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 73
        },
        {
            "id": "bebc0994-4290-4fa8-8e55-d028a2681c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 74
        },
        {
            "id": "7e380f3f-d6be-4be0-9194-ee612486f5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 77
        },
        {
            "id": "a0116249-e935-4950-8343-3fb6d19590ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 78
        },
        {
            "id": "303c7e89-47c7-4612-9986-d77c20557479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 79
        },
        {
            "id": "4743c387-25cb-4f53-b5c9-c00bed316b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 80
        },
        {
            "id": "464f7c3b-eb66-43e8-bfd9-7c7c6666dada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "7c29b9c8-42cb-4838-81ce-c28c8c0e834e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 85
        },
        {
            "id": "693675e6-0623-4bf0-a9d3-9ccc2e83a4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "c73ff527-fae5-44cb-8abe-c853b5f2c703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "cf9618d5-6739-427b-8d13-4e02564f96be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 39
        },
        {
            "id": "f95b3719-34f7-40e8-b502-dd9b0876332b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 68
        },
        {
            "id": "82a4efe4-5350-4af1-8c3d-b12e6aeeff0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 72
        },
        {
            "id": "7241c40a-7b80-4091-8c82-1df2d36f4521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 73
        },
        {
            "id": "dbafb899-7f1e-4985-af5e-5b3362a56517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "60a6895a-87dc-4834-a0bf-44cc614d7afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 85
        },
        {
            "id": "07c884ea-b0f4-4d85-aa9e-f901ffef3bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 86
        },
        {
            "id": "3fc6ca4c-f2b7-4d86-bdc8-2894536ade12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 89
        },
        {
            "id": "78e79684-92a2-47e6-bc74-92c004eb1b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 39
        },
        {
            "id": "81637399-7d37-4c0a-b064-8501d9fdb23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 65
        },
        {
            "id": "da25e404-e06d-4e78-85a4-85cd2ffa52dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 66
        },
        {
            "id": "6cdd651f-58e9-480d-a8b6-0b4d02a7b55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 68
        },
        {
            "id": "849f5e11-f4e5-40c3-8cfe-8c0e923b7b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 69
        },
        {
            "id": "f2c7c942-d467-464f-982f-a3a9126e19e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 70
        },
        {
            "id": "88a38c4a-e2ae-4769-8bbc-229f2b48ca9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 71
        },
        {
            "id": "6d1fedd5-c097-48ed-9158-35d50f4a8885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 72
        },
        {
            "id": "3d7cca66-b759-4530-a9de-b3ed19572e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 73
        },
        {
            "id": "8eeb7940-821c-4288-bd6d-54827547e800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 74
        },
        {
            "id": "758d4d5f-6fa6-4dd8-93d4-5b4a223a0c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 75
        },
        {
            "id": "5768cf62-0bde-4008-ac41-01848a828906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 76
        },
        {
            "id": "60fbb53f-6743-4c7a-8d98-0955f88e1ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 77
        },
        {
            "id": "cc53b308-5ef1-4b20-b01f-7261c7e3c20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 78
        },
        {
            "id": "abfccaf3-2b24-4934-9893-de03e4423653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 79
        },
        {
            "id": "c7b77ffb-c555-46d3-a14b-0813ddbae087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 80
        },
        {
            "id": "4cf9e564-aa6f-4fee-a3df-987b9fb770fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 84
        },
        {
            "id": "8101e215-84a8-4a3e-9291-2b1ccbe21fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 85
        },
        {
            "id": "c2e434ff-b768-4313-8e42-811141b13811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 86
        },
        {
            "id": "5070f3a6-0a43-4ce9-8e08-bfd743360b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 87
        },
        {
            "id": "fd5ba18a-a403-4017-966f-8aceff46861d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 89
        },
        {
            "id": "6443c62a-d902-4b27-ba31-97455fa3e6be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 39
        },
        {
            "id": "f60ce11f-9458-443c-8490-82f9b12173f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 73
        },
        {
            "id": "40f9d0c2-37c8-49df-b997-5e11867f71d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 84
        },
        {
            "id": "c32f823b-d726-4c21-8cb1-c1aa1ed6bcbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 86
        },
        {
            "id": "aa5b2fb0-724c-4f52-97ab-4834bae50955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 88
        },
        {
            "id": "1eb78a4d-76de-4b20-81e3-e55b08daee49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "2360c316-0502-4b06-8b80-78da8253ffb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 90
        },
        {
            "id": "638ae1a3-3646-44e9-90d8-d07a6b7f244b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 39
        },
        {
            "id": "14027ce0-c5f4-4bf2-b354-d282656b7945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 67
        },
        {
            "id": "2b214ce7-7e1e-47bb-9502-d2bec80685fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 68
        },
        {
            "id": "a94f7b75-ec9f-4257-8d6f-bb1dd8be4c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 69
        },
        {
            "id": "3eba8a0a-4b2f-4e62-9248-f271bae68534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 70
        },
        {
            "id": "7a170c05-e1b4-48c1-a8a1-9533318c81b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 71
        },
        {
            "id": "f74fbe4c-aa93-490c-9e83-463fc61b57a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 72
        },
        {
            "id": "05bc7a2a-9ef5-4efa-99fa-eeb67186f407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 73
        },
        {
            "id": "c9813c39-a999-48a9-9ed4-f6f615acf75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 74
        },
        {
            "id": "f37687bd-29c2-4102-b9ac-cbfde1961747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 78
        },
        {
            "id": "c6170998-4759-4808-a8d8-5b95c771fad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 79
        },
        {
            "id": "6f3db7a7-ea5f-4bec-bb03-46f04a887ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 80
        },
        {
            "id": "238dedef-3c3b-425d-9527-1b1ffd37dfa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 81
        },
        {
            "id": "a3ad5fa0-dbbc-4bcc-beac-cd4b90db2bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 82
        },
        {
            "id": "0d8195f7-567d-4929-8727-e3b67f0482ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 84
        },
        {
            "id": "43398f47-4591-4eee-83a3-2bf91e92df84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 85
        },
        {
            "id": "dfa7f625-f0e3-4ebc-bb22-eb8f346ebd89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 86
        },
        {
            "id": "0c327da4-1e2e-4cbd-8e79-b560c49cabd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 87
        },
        {
            "id": "1dbdb960-be9e-4bce-806e-3b1fa4bc1cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 89
        },
        {
            "id": "1c75470b-f03d-4099-bb7f-cdabc1285670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "99d4b6fc-ebc5-4deb-a824-cec82804a86f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "deb22581-6055-4aed-843d-8af108f70ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 71
        },
        {
            "id": "7944cddd-beb2-44ad-a626-f16504b7ed07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 74
        },
        {
            "id": "51fe1f78-2e9a-478b-824d-2c6c4f31f25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 87
        },
        {
            "id": "7628e613-a5d7-4f27-ace8-6f10c5fac905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 95
        },
        {
            "id": "1e006cb8-93f0-4ad2-b95a-d89650d93555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "941ddbcf-5272-463f-81e6-05c7645bb544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 89
        },
        {
            "id": "997ef8d7-501c-4917-ad5a-99887b69d9a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 39
        },
        {
            "id": "028ff0bb-8a78-4c01-809f-ffafef65cbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 73
        },
        {
            "id": "76bdf81d-76b0-420e-917b-a6e482425eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 84
        },
        {
            "id": "11e97a2f-8402-47e4-b0c1-ce2d94962544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 86
        },
        {
            "id": "db6d27b8-bb73-4fd1-91a9-ce520a2411a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 89
        },
        {
            "id": "a9ab3630-30c9-499b-827c-e39906d2448d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 39
        },
        {
            "id": "bb3f9585-804a-4548-b8be-ab5ea55ec997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 55
        },
        {
            "id": "6120ea76-53ca-4f0f-bd00-1414c722e937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 63
        },
        {
            "id": "d7e7a656-8a72-4800-8b32-42610d5c457e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 65
        },
        {
            "id": "19ada418-020e-40f7-9148-0a7409277b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 66
        },
        {
            "id": "4bdcd2b8-a192-45ae-a9ef-afd549fe12f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 67
        },
        {
            "id": "aa9175bc-0dd3-4f10-8a3d-2193a80854d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 68
        },
        {
            "id": "8f790eaa-3d25-4b0e-a373-f422c85f7983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 69
        },
        {
            "id": "204f60cd-4814-4678-a282-36e090bb33c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 70
        },
        {
            "id": "fa9f3c6d-0b23-4e88-a81c-b67ebf50cd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 71
        },
        {
            "id": "9542b01a-ee14-4d71-9d66-4dba32bad47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 72
        },
        {
            "id": "87b95da4-f100-4e21-b154-6b33374c733c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 73
        },
        {
            "id": "e171da06-41e4-4d4c-885f-6011d11752f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 74
        },
        {
            "id": "1e4b3a61-4f9a-4c95-81db-d3f0e2e2aff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 75
        },
        {
            "id": "26cd608c-ee98-4527-89c9-ed522e15b23f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 76
        },
        {
            "id": "3b8ea4f6-de7b-41ca-823c-055cdac9d27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 77
        },
        {
            "id": "aa729b5b-9a63-4909-a7e5-94f7deb229a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 78
        },
        {
            "id": "d665a92c-5799-49ac-b139-a4efc61de4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 79
        },
        {
            "id": "e041321a-906c-4abc-b4a5-6738c95b4491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 80
        },
        {
            "id": "9beb9c07-dd2e-4508-8518-49bea5dc4aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 81
        },
        {
            "id": "9d814808-8908-4fe8-b8e6-693d5e813909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 82
        },
        {
            "id": "ca89bd20-3966-452f-abe9-ae7d934730cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 84
        },
        {
            "id": "7e32333c-9dea-4c41-a66a-53916b01c61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 85
        },
        {
            "id": "d45567f0-a8cf-4cdf-bf2d-5283e70250e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 86
        },
        {
            "id": "fef073f9-02c9-4047-bf41-a5f9b464694d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 87
        },
        {
            "id": "4ee50b57-0070-40e0-9da3-4eaef0157298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 89
        },
        {
            "id": "b01c44a4-4988-493e-9a8b-eda974f91fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 39
        },
        {
            "id": "b624407b-6b91-4c12-b713-9a3e262fc9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 73
        },
        {
            "id": "fa0f61ed-ebc9-4b82-9799-fc80e5b0f816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 84
        },
        {
            "id": "8b02537f-90cd-4f98-955f-55e1c58df141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 89
        },
        {
            "id": "eb70d1a5-b96d-4e1e-8a53-d446cee405b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "9af189bb-8c9f-4a11-bb1b-3e4fc35d9894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 66
        },
        {
            "id": "a5f8b03b-4cc6-4be5-a73c-3618c3c104a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "0438cd1e-5085-46c6-88a4-e33891b844c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 68
        },
        {
            "id": "3fc50d4e-9ef0-4e1f-9cf6-a6a46a7d8c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 69
        },
        {
            "id": "fde9c11e-f807-4948-8284-61f2d2a8eb42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 72
        },
        {
            "id": "afbd2b14-daaf-4914-ba29-3921fe8306f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 73
        },
        {
            "id": "193cbdd3-2ea6-4540-be89-8e0f260229ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 74
        },
        {
            "id": "b6b36da3-5bd0-42eb-b6a6-a34e9bed227b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 76
        },
        {
            "id": "f616c5ff-a985-4677-acd0-362b72b42d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 77
        },
        {
            "id": "c7f54df2-1f34-4d25-82a2-363217ea5714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 78
        },
        {
            "id": "27b21a01-f2c9-4c2a-983a-6c0e5ddb34e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "8af545fe-b669-4f63-b78f-f74fbadd5c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 89
        },
        {
            "id": "fbf1e117-1aad-4dc2-ad3e-2511d6c3549e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 39
        },
        {
            "id": "0bfa273d-5904-4da6-84a0-49cc8117aa31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 52
        },
        {
            "id": "373eda42-2de0-4153-b796-bc98e3ae4fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 55
        },
        {
            "id": "4eebec4f-5037-454b-a090-a73543d2c9a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 57
        },
        {
            "id": "5c7ad169-df96-427e-87fa-d06f0dc88cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 63
        },
        {
            "id": "10e4c30a-2580-4ac5-bf0c-baa543e63f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 65
        },
        {
            "id": "fe7e2dff-45ca-4c6c-be59-9459fc6929c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 66
        },
        {
            "id": "8a06415f-2e1d-438d-86a8-59ebf3708adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 68
        },
        {
            "id": "c0f9290e-912b-4d43-a2c5-92e366cf2211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 69
        },
        {
            "id": "c3bd90db-24b5-4d9e-a834-062d39200005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 70
        },
        {
            "id": "2a9f6aad-5389-4d4b-988a-97d934de4b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 71
        },
        {
            "id": "4d81d736-7866-4e80-bed8-626d952b656e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 72
        },
        {
            "id": "026cabc8-c2d1-409a-be1c-23d0b8993038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 73
        },
        {
            "id": "b15627f4-452f-41b4-941a-7ba5de4bcc17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 74
        },
        {
            "id": "d0e678c4-bc55-442e-816c-4a067d570ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 75
        },
        {
            "id": "05439bf2-48fb-4134-8efd-31268f07585e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 76
        },
        {
            "id": "1ca80e70-2c12-4ea6-96f6-a1644ffd192d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 77
        },
        {
            "id": "4dec5720-72aa-457f-8262-ba999c32835e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 78
        },
        {
            "id": "2a942f2c-420d-4957-84a6-66ffa1c82194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 80
        },
        {
            "id": "030191dd-e3a1-4175-8ab5-8ddfe83108fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 81
        },
        {
            "id": "a9aed2ca-adcd-4c81-9559-9bcfdbe7c9fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 82
        },
        {
            "id": "48ab8a29-7c2e-40ef-8bd2-9931d469f1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 84
        },
        {
            "id": "b5b3ce09-935e-49d9-b722-bff7fc1aed4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 85
        },
        {
            "id": "7714d463-fd63-4c62-8425-e334c3427271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "86d3d23f-3b6e-4245-b3fb-6683832d5adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "25792540-cd0a-4dfb-a1d7-6f371a69601f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "8832b275-1ce2-444b-a537-9fdf1cdc6e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 44
        },
        {
            "id": "3d5d828e-ba22-45c9-b17a-df082576856e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 95
        },
        {
            "id": "60b261eb-ab6a-476c-b29b-5a5b24447e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 39
        },
        {
            "id": "91d041f9-0ddc-4dc2-98dc-9e8ad942e49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 72
        },
        {
            "id": "742d1f23-00cd-4de4-b562-724ec209c4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 73
        },
        {
            "id": "8dd8cabb-a7ef-4dea-a178-b53ca934e8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 75
        },
        {
            "id": "b4a0f1cf-a74a-4b6f-ac31-9a2f04666f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 84
        },
        {
            "id": "ed5ece53-4056-45d8-b82a-13c6d7de7c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 85
        },
        {
            "id": "37ea987e-8255-453a-8fe7-0cff70cff1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 86
        },
        {
            "id": "22ad864f-4d38-4ec0-a229-93c8ee6a5e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 89
        },
        {
            "id": "77174989-fa17-423b-8030-01db5388550c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 89
        },
        {
            "id": "fa71f95c-4991-4ddb-8eaf-98a231f9015c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "eea4e12a-e8c0-4601-b0d4-83664e4cdcc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "a13ac0d2-4758-4aab-9d61-ec72385fafb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "6579578a-14d2-494a-81ba-0ed0d5ce4ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 95
        },
        {
            "id": "0c08e76d-9696-4d4b-8f74-662d2b2a0366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "36e66b69-d68b-4a46-b913-c3d2a43aa025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 72
        },
        {
            "id": "ce53b8a0-0d7b-493c-9f1a-48bdc170f3f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 73
        },
        {
            "id": "932690b8-540e-4c9f-baf4-f88c268bc8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "079fd158-e906-44c4-8db9-d23a38668f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "7dcfb6e6-ac87-4c76-b8aa-75224cde560f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "31eff51d-20a8-4052-9ebd-17e4d11703e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 71
        },
        {
            "id": "66462a78-dfd9-4c7b-9dce-ba1a8b7be56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 74
        },
        {
            "id": "a18a76f6-8931-47db-96a9-453da3e291e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 95
        },
        {
            "id": "31cc41f4-76be-4b82-aa31-caeed4454d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "0d61f1f8-fde1-4903-9459-69b1e2d004ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "cbc4cd83-0266-48b5-b3a0-0dc2f45bdfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "eb7d15ef-c840-4148-ba74-efdeca14a02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "ffb106ac-ce41-4345-84dd-42f862a9661a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "36d984f9-89ba-4142-b59d-f23cd28ff8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 106
        },
        {
            "id": "80ead3be-9c90-4cf9-883b-da45ff2c3ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "15c13583-669e-4c34-815e-741e3323ada3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "5277aac7-3d30-4c75-a5b6-7809cd5ec01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "d0570c09-d3e1-4a59-aa6f-85d5194c634d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "275be69c-a740-4d8f-9d50-094e160622f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "21318b05-7cec-46a1-8974-8917762a0e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "32c28b2c-555e-41bf-a445-174b4d5a4b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "50f78dca-7716-45b2-8d91-c558c932ca1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "9ccb3916-e3d2-4d2e-8aae-cf42936fa582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 118
        },
        {
            "id": "8bb5e578-f453-45ef-ba31-eaa5041463c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "c7b63f4c-471a-40c2-bbb7-af7a140ce31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "1d58ac6b-be52-45d1-997a-6db713b1cdc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "e79ba192-8981-4647-97f3-2a0e92c0e16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "f9733b9d-2edb-4c9e-bd90-9621a574e2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 39
        },
        {
            "id": "6f2929ee-b368-4e20-8f14-b68f5d0cd7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 73
        },
        {
            "id": "76c6b57d-3442-4e9d-9864-aaa141da71ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "f53b3066-91a5-441c-a28b-5423fda560f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 89
        },
        {
            "id": "6bfc083a-96c0-430e-8a02-6f0ca5ae8fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "14be8c74-8120-4dc6-b8fd-7eab1c649d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "dd1993a4-3267-48f4-b48e-ef466157f7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 71
        },
        {
            "id": "ffbb986a-7837-47f9-941e-39544cba61d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 74
        },
        {
            "id": "a25648a7-e80b-43f0-983c-7a7bfecd44d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 95
        },
        {
            "id": "399f44f0-a2d8-477a-aec5-41fe64282459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 84
        },
        {
            "id": "7d728505-4a67-4146-b5a4-dd973b4357a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 74
        },
        {
            "id": "38ff1461-2128-43b5-9141-89a6b000b446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "93b33908-1341-4947-ac8d-c0b29ba31847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "fb54db83-1c3e-484c-9c8e-519c975b67a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 71
        },
        {
            "id": "b444452b-0b92-493b-9a44-3240c1d2cfb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "f1b9002a-d1e7-4345-848d-2c0f6b51bfdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 95
        },
        {
            "id": "b9e4ccec-dffc-4a83-9f1a-c2db7225ad21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "d8516dc6-16a3-4c04-b599-2356f84ca6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "99ce4fc8-1147-4f8d-8008-8d4f1e930917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "a75e84a5-9abd-4426-b3d5-2f8aee36435a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 103
        },
        {
            "id": "a5fe8a5e-244c-4b59-acdc-6ec4bd6d89ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 106
        },
        {
            "id": "54271328-3253-4736-b6d8-60bc8409e7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "81946fdb-ad93-4876-a713-d4aa7e35f021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "4d2ac526-a144-41dc-9817-c920f64a09ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "280e5e0e-f4ba-48c9-8077-2583a2fe30e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "9d8f6e93-b64e-4c2f-9f39-e35175354faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "c4236e02-5561-4eee-96d4-9e5146227ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 119
        },
        {
            "id": "16b6418f-eada-4dae-a060-79ba340035a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 121
        },
        {
            "id": "d5b4b8dc-3a15-4674-a9a5-1f571b4f8e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 65
        },
        {
            "id": "acb216ed-a210-4538-b4e1-ce30b5db84b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 66
        },
        {
            "id": "69dc60ec-883a-425d-a169-e0fe8db3a825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 67
        },
        {
            "id": "2b4c2198-6407-4c13-9dec-23684578384d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 68
        },
        {
            "id": "f09bcffa-6ed4-45bf-935f-e52699500ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 69
        },
        {
            "id": "6d069ed1-38d8-446c-8fca-b5a98acca272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 70
        },
        {
            "id": "d0b61d9c-1b9d-496b-a3c6-969e92de88b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 71
        },
        {
            "id": "8a2b851b-1249-4ea7-9fa8-b1d9e80ba803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 72
        },
        {
            "id": "52db3d19-89b0-4b7c-a2bd-9e5d1d1c2716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 73
        },
        {
            "id": "2476fa0e-434b-46cf-a3fc-676412f094f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 74
        },
        {
            "id": "e97e196d-ea26-4b83-8611-d8f08fb1388b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 75
        },
        {
            "id": "369e3533-26ff-4aed-8643-b850eecc0341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 76
        },
        {
            "id": "5a82538c-a317-499b-92d0-07ce8a50bd9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 77
        },
        {
            "id": "988c4d1f-6064-4bca-a6c9-e20a0369101a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 78
        },
        {
            "id": "32ea5fd8-12f7-43ff-a6e9-1d35971a474e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 79
        },
        {
            "id": "718736d8-046d-463b-bda5-348a6ce9a5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 80
        },
        {
            "id": "d8f347e0-b16b-49c6-9996-ed779042d22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 81
        },
        {
            "id": "6b21d9c6-b7b5-4697-bdf1-6baf256c8aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 82
        },
        {
            "id": "ba77f66a-586b-411d-9862-8ce0cd07a924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 95,
            "second": 84
        },
        {
            "id": "1a051f41-9f42-4c68-80a6-c92d404e15ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 85
        },
        {
            "id": "0791bf8a-0fab-4bfc-8de6-306f5e2f5710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 95,
            "second": 86
        },
        {
            "id": "35706ccd-2b18-4829-87be-44c856bb0050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 87
        },
        {
            "id": "46ca5b97-ab80-4531-85f1-aae7b026c00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 95,
            "second": 89
        },
        {
            "id": "fb8392b9-ea72-46e3-b392-9d7dddb778d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 97,
            "second": 39
        },
        {
            "id": "5609622c-a935-4fda-94cc-2dff028e0de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 55
        },
        {
            "id": "a610d5f8-fc08-4fb4-86c0-afab3d2a5064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 97,
            "second": 63
        },
        {
            "id": "6efe2751-71a8-4e79-902e-ce444ffb7ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 97,
            "second": 84
        },
        {
            "id": "fc119003-1dda-4215-a1ee-dfa7c430ceed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 89
        },
        {
            "id": "d3d7699d-b8cc-4a3f-a36b-014f9b16a82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 98
        },
        {
            "id": "9ac02bff-57b6-4eb6-8ecd-dc9e57afe153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 106
        },
        {
            "id": "3496d1cc-b009-4c2e-af66-7e976386d7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 108
        },
        {
            "id": "abf7975b-359b-458c-9e69-2de93f46ee97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 118
        },
        {
            "id": "7c0f0024-fd19-4e84-b6be-41cc9bbd6e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 39
        },
        {
            "id": "2d2cfa93-34cf-409d-a155-7ae9ff40aabc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 63
        },
        {
            "id": "148e92bc-17f1-458d-9612-dd269be688ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 84
        },
        {
            "id": "477b7bac-dd29-4167-8f2e-3a585cdd54d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 89
        },
        {
            "id": "2139d0c0-ded0-4a7e-9bac-0bb119a1e95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 106
        },
        {
            "id": "c9d7a57d-b654-47f7-94cf-dc46510c50a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 39
        },
        {
            "id": "f6e7f030-13b6-4993-9c33-40c9953be99e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 63
        },
        {
            "id": "7845e4b5-b200-4dc2-985c-8ef724046868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 84
        },
        {
            "id": "60b80560-d5ee-4911-ac96-253a4ec38dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 89
        },
        {
            "id": "5acd223d-1a33-4a41-a481-1bb75f63ff7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 106
        },
        {
            "id": "687ebd7f-6873-425e-8833-d0ea65a15483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 106
        },
        {
            "id": "947106f3-dec6-4c82-8b85-1b1e4c1f7793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 39
        },
        {
            "id": "2cb2f6c2-f69a-454b-a174-3b46b77122bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 63
        },
        {
            "id": "b4013713-db8d-431c-b006-df057cd16391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 101,
            "second": 84
        },
        {
            "id": "c2dd7c84-1155-4732-ab25-76efa7a2f30a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 89
        },
        {
            "id": "451c0402-6e89-4f95-a539-45dc7bc88222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 106
        },
        {
            "id": "82b90a62-5dd8-42f3-bbd2-90f61a1700c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 97
        },
        {
            "id": "79b9b5c0-391e-42bc-9ff1-c0dac53d329e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 99
        },
        {
            "id": "b31100f7-3ad8-4bd5-90eb-d2bdf4151db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 100
        },
        {
            "id": "68d88794-3f5c-453a-a2c2-0ba52ec998e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 101
        },
        {
            "id": "8e140d4b-f2b9-4e4b-bb08-47454ee9bbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 102
        },
        {
            "id": "3ea99eca-34f5-4ed5-87c4-70828343bb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 103
        },
        {
            "id": "0cc38933-2ede-4823-91d0-a59ee1c1ba35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 106
        },
        {
            "id": "c6b62170-623a-4b93-b2b9-f286ac641ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 109
        },
        {
            "id": "5f64bf02-92c2-4614-9fb9-1f037c9a0232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 110
        },
        {
            "id": "abe32885-a873-419e-8d56-d05454e94009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 111
        },
        {
            "id": "0f257b23-0e25-4fb0-a65f-ba49c5a22057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 112
        },
        {
            "id": "f9edd7c6-50b9-4cac-8024-b40cf033327a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 113
        },
        {
            "id": "189ee886-c90a-42dc-acbd-a63e15bcacff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 114
        },
        {
            "id": "8c411aed-c29e-4c9e-bea6-c87dc5bd2ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 115
        },
        {
            "id": "e5ba136c-91c0-4eff-881f-ead9f268baa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 117
        },
        {
            "id": "ea0804a6-0126-4070-8fa8-4f72e81900bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 118
        },
        {
            "id": "8fbe5b13-e85d-4d62-b196-c3f3873be984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 119
        },
        {
            "id": "050e7f78-97c8-4d03-b9e4-4a057b5c89f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 120
        },
        {
            "id": "b1bab474-5ac3-4618-894e-d36fffc99e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 121
        },
        {
            "id": "1b12eb42-e016-4101-bbbc-3cc6d529550b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 122
        },
        {
            "id": "9fb42090-dbd4-4dc9-8636-3e2c8bb6a512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 89
        },
        {
            "id": "7ea096b5-2b1a-4f9d-ab64-4c3db882b947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 39
        },
        {
            "id": "b9e7001b-fafa-4961-a248-f7180d59a83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 63
        },
        {
            "id": "49072330-cb84-4841-a73b-6fc836c562af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 84
        },
        {
            "id": "eb73168f-01df-4c64-b40a-c2fe7edbd7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 89
        },
        {
            "id": "a72b7542-68d5-4e89-8458-22a3bb6f5dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 106
        },
        {
            "id": "72192a34-4b62-4841-ae68-868f362ecb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 39
        },
        {
            "id": "efc43266-e62a-4d34-8047-6746219331b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 55
        },
        {
            "id": "23f053ef-de58-4741-b738-195663b2b605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 63
        },
        {
            "id": "ce15d078-934c-489c-90ab-ec26a22bbc8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 105,
            "second": 84
        },
        {
            "id": "777783d2-2b24-4d83-a36f-9f9ef423e55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 89
        },
        {
            "id": "17e95d6c-2d36-4fa3-964b-fab63ed00360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 98
        },
        {
            "id": "356f47fb-7ad1-4469-9c52-3587f5f7bfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 106
        },
        {
            "id": "ca85f81b-fa13-4af4-b40b-ceaa38f7ec08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 107
        },
        {
            "id": "7a05028b-d606-4d51-8f2c-d4e6821a666b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 111
        },
        {
            "id": "39105c2f-dc98-4bce-8575-82a7837ef8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 117
        },
        {
            "id": "c2b4ec3c-6677-4089-a09f-e97212f24f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 118
        },
        {
            "id": "1dab6657-3d6a-47c3-99e9-cbed74662aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 39
        },
        {
            "id": "79881342-650e-4424-aadb-913e30a7b036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 55
        },
        {
            "id": "5d089af4-1049-442a-9e2e-a5c8f5e7f7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 63
        },
        {
            "id": "195467fd-b87f-4065-9f93-665390d6761c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 84
        },
        {
            "id": "091e8fef-68ea-4984-9744-de804bcd94e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 89
        },
        {
            "id": "07de83a4-4495-46e8-83bf-8021bb3c967b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 118
        },
        {
            "id": "b4ac7c19-5bfd-4cd9-84c8-f18825346e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 39
        },
        {
            "id": "3d4dc353-9c8e-4b86-aa17-df02744e97a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 55
        },
        {
            "id": "7769d55d-03cc-4643-9602-d0e4fde0862d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 63
        },
        {
            "id": "f9988753-c7c5-4d01-ba21-ef194b91e691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 84
        },
        {
            "id": "20b15623-e127-4fd1-ac4f-5d8c27b35994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 89
        },
        {
            "id": "041ecfed-0329-4bc2-8da2-542d567de6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 98
        },
        {
            "id": "12a44b7e-f5b5-4a66-a3eb-ce3fab8c921d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 100
        },
        {
            "id": "e57bab20-b804-4a4c-8ad0-37cb440579fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 101
        },
        {
            "id": "f4e76e2f-60b9-43f5-8b11-e3a7d5888e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 102
        },
        {
            "id": "353de2ed-c53d-455e-b4e2-91e428939de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 106
        },
        {
            "id": "8e05a176-d99f-4985-b671-d23ad1054368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 108
        },
        {
            "id": "770c5b08-5b90-4770-9b3d-6600d627e4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 109
        },
        {
            "id": "5c74c30d-f70f-474c-91b4-73770ddbae6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 110
        },
        {
            "id": "2069fd6f-e2f5-4f7c-a9c5-b74d49b58487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 111
        },
        {
            "id": "815d6da6-4184-4261-ae63-bb96c036c349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 112
        },
        {
            "id": "387f5986-b982-44ce-ba8d-70fcf9eef17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 113
        },
        {
            "id": "97e9a472-1540-4c15-a334-2edbf3870fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 114
        },
        {
            "id": "1fdc49ed-ba07-4366-885d-520341164616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 116
        },
        {
            "id": "33376e0f-b949-4576-b873-0b1fcab229c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 117
        },
        {
            "id": "caf66590-4605-4a11-be9f-7e6bab8f43b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 118
        },
        {
            "id": "d1029590-ec72-4687-b31e-5f62e0e75968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 119
        },
        {
            "id": "0c032d2e-76a4-431b-816b-f743b34fbcb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 39
        },
        {
            "id": "39193f0f-85cf-4fed-b224-bb7aebcb989c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 63
        },
        {
            "id": "e43b9355-b465-41aa-a074-8321cc6d538d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 84
        },
        {
            "id": "e805e1ad-5dd0-4fc7-b9c3-e0db2756c0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 89
        },
        {
            "id": "6faae8cf-6e6c-44b3-bd0b-5aef97d1c1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 106
        },
        {
            "id": "036be727-9212-4e0d-9504-c717a11654d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 39
        },
        {
            "id": "67741b82-9f23-4392-81af-42de7b2bfe8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 63
        },
        {
            "id": "b1f811b2-e06e-4877-b0ff-0ad14e900155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 84
        },
        {
            "id": "979962bc-8793-42a5-ac9b-59fd838fd971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 89
        },
        {
            "id": "c56a844d-46b3-4acb-bf7a-fd9418c632fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 106
        },
        {
            "id": "9f6b2c7c-6637-4db1-ba6b-3413424dc0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 39
        },
        {
            "id": "459709cc-e82b-4b4e-b1ec-700a80327c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 55
        },
        {
            "id": "98ab2b35-a052-4d48-8feb-e38e4ff092e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 63
        },
        {
            "id": "626071cf-f31a-45e0-bf7f-ba5469962fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 84
        },
        {
            "id": "d408ffe6-dec4-4f07-8a6f-4a82387bdaf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 89
        },
        {
            "id": "acccc584-0a76-4f77-a19b-13ce3b18712e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 106
        },
        {
            "id": "781296d5-c993-422a-a98b-1095baf9081e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 39
        },
        {
            "id": "03ba92ed-d2b4-4c9a-a5e5-c1d32514545d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 55
        },
        {
            "id": "6ec146f8-6deb-4bdd-bb77-001d1c3debff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 63
        },
        {
            "id": "7371112a-a347-407d-aa3d-b0f66f0f5219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 84
        },
        {
            "id": "109f8dab-4c68-4326-9268-012869c6f01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 89
        },
        {
            "id": "eb306786-aa8b-4139-8d7d-4cd2ee8a059c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 104
        },
        {
            "id": "948736d6-86b9-48bf-9789-f9e51e39f41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 106
        },
        {
            "id": "15d9c582-ec03-4d30-bf55-940d18586278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 39
        },
        {
            "id": "e991da58-6407-4ffa-b97c-5f3fa7fd0d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 55
        },
        {
            "id": "ca3f0add-53e4-4e6c-a022-288cf198a9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 63
        },
        {
            "id": "099ea990-f700-4e26-9efd-4dc4088bc5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 84
        },
        {
            "id": "f0fa6936-f82d-4bd2-93a2-38c2227a724b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 89
        },
        {
            "id": "035b7703-72fe-4221-8b09-098cdafaec4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 106
        },
        {
            "id": "70aede41-c897-48d7-ac45-f96dd748bb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 118
        },
        {
            "id": "af481220-ec3e-44a2-a9af-bdb7fe836ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 106
        },
        {
            "id": "bd9fb428-373d-4028-9307-3ed6a4698b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 39
        },
        {
            "id": "66bd07d6-76a0-46cf-8f62-0996012a8137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 63
        },
        {
            "id": "c3890488-eed8-45f7-a214-fca468217290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 84
        },
        {
            "id": "5058cbc3-16c8-4ede-880e-e1def24ac4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 89
        },
        {
            "id": "a84b16b3-2257-45e1-b829-d1bf6f8e7b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 106
        },
        {
            "id": "9e4607d2-91c0-4d49-8fee-fb91f839ebe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 39
        },
        {
            "id": "2e9510a3-61f5-48a8-ad15-fcfd98d35307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "7ee1cddb-db64-4f91-865f-b6715b81fd44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 63
        },
        {
            "id": "24baf821-e800-434b-b400-05eb92e4a389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 115,
            "second": 84
        },
        {
            "id": "7e937220-5fb2-4e8a-b08f-46753377b8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 89
        },
        {
            "id": "e5754c85-68a9-4580-83ad-d7529415db6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 98
        },
        {
            "id": "eccd0bff-4f7f-4374-b808-a367f2d920dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 104
        },
        {
            "id": "22d2f200-fdff-44aa-96b9-683b2c6bb881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 106
        },
        {
            "id": "b834304c-cabf-4c64-88bf-6fe2e7d6fd9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 118
        },
        {
            "id": "3489aeca-7aa0-42c6-84b1-4903c4cd0c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 39
        },
        {
            "id": "798f6bfd-be08-4451-beee-6171e01e87f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "da6d1a07-0267-4f46-91ba-26f1a2e8d504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 63
        },
        {
            "id": "5183bb8d-b241-4959-a776-4d2774c00ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 84
        },
        {
            "id": "773c4d15-2646-4e11-a846-a1f1a3b0b516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 89
        },
        {
            "id": "f774542b-e1fe-4921-8604-8e44021f3393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 98
        },
        {
            "id": "c4222846-a298-4d29-8d4c-cf0f19932a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 100
        },
        {
            "id": "98e45f32-6394-4cb0-96b9-843f6745ba4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 101
        },
        {
            "id": "2526db07-6dba-476b-beb3-b72cc206b489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 102
        },
        {
            "id": "25454821-b152-4894-bb94-7d8c80e6c7f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 104
        },
        {
            "id": "cbb038c7-9242-4a20-8623-b219b16cc837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 106
        },
        {
            "id": "4d9ce2bc-eaf4-4f77-97c4-41d4948a749a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 113
        },
        {
            "id": "1ad4c1e4-3cbe-4faa-b782-43b184b6e3e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 116
        },
        {
            "id": "495238f2-39ba-4785-b622-374aa0e75bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 117
        },
        {
            "id": "356ecf97-517d-4a6e-b3b2-5d0050838c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 118
        },
        {
            "id": "4bd87699-1a1e-4e83-8508-c77d10d6a9f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 119
        },
        {
            "id": "708156e3-4e37-42be-94f1-881421d93805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 106
        },
        {
            "id": "d44eae39-c280-4199-9b41-44ae33881360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 106
        },
        {
            "id": "45c9b19c-d43c-4dd9-8c23-258c49ee0a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 39
        },
        {
            "id": "089632fe-43f0-4c2b-9c09-da4e42d3c647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 63
        },
        {
            "id": "36d7153e-e2f8-4e4c-b2d3-d9d13fa8bc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 84
        },
        {
            "id": "e6cefdf5-79cf-4cf8-8b71-aca84515b460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 89
        },
        {
            "id": "ff6aee2f-dbaf-4bf7-80a5-a991b29f020e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 106
        },
        {
            "id": "e304e624-2969-4b30-bff4-a1766d523b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 106
        },
        {
            "id": "d92bc5b6-cca7-482b-87f5-d45f68118ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 122,
            "second": 106
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}