{
    "id": "f1ee26fa-bde1-4d32-925f-62deda38f9e7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntUI",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ChevyRay - Slapface",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dfcde597-9fc8-483a-aafa-ad1729276b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5baf1ccd-1f79-431f-83a8-0b1a48ef83fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 222,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "72d1a49f-90a2-44db-be49-29929c54487b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fa04c53d-f3df-40b0-b539-c8733526d240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 197,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "112e4e9e-c26e-47a2-a3b4-ed034983db28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 185,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5e686a9f-65ec-40a4-9fff-8a55f83dc223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 175,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a210f88f-87b2-4b68-90f2-a1183e3d1a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e948968d-57f1-4e3d-a3d0-d9280237501d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 156,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "edbb7cb9-01f9-4307-a9e5-8c36c6cb81b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 149,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9fcf2ca4-f2ff-4fc7-99e1-743d725b3a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0543d136-9025-4e5a-a3ae-4eec97de392e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8a1c85f9-ca81-46e3-bcad-83683504da66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 130,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cacfb3f7-a7d9-4e9c-9d82-3cc5d43a4a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f4ebc2f3-d3d0-4871-ac09-1274a1277241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "16559fe5-57fd-4dea-ad44-93e14090b72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 96,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "87b801a6-6446-4dda-9d91-9ae536970756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fac453e4-a54e-4bf2-853c-3e2a73403a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5b5c1254-11a8-42a2-b202-4c3aa38de767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f1e8f2fb-9438-4498-b494-808f877d9eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "61b866ed-2f96-4cbd-b3ac-494323dacc02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "848baae1-9700-4db1-a4a5-1e7dc5a4c5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 27,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "11d7d785-3e52-421d-b068-0d0106f51504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 118,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e05ff3d3-f464-46ce-a19c-39a7b449cf6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 237,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a8e4a6f4-b265-493b-8a54-39dc47764eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bfc85b6b-f063-4431-9fd2-753ee0afd67f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "22f89b4d-ff70-4f46-98e6-ccd156b7c0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1300cf7e-bfef-446d-9278-a23f63f93a24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 241,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2260f70d-1c19-4837-90ec-f9f1839891e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 235,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "16126c67-d8f6-4090-abef-58aed4cb173b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9fb609f0-50e0-4511-ae86-1dd572bb79df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 214,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "123912a8-9188-4144-818c-5af5d5bbcd85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2eb07aa4-205c-41f6-ab8b-cba50a84878f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 192,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4c83aaac-6354-4de2-8370-cab4e1ee7845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7bcdd3ec-62ec-4cb6-85eb-646ef4620e90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 165,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c088868f-a31b-480e-a165-8ecc20a3ead8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 153,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2e31e44a-450b-443f-8022-9cb7a786a59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 142,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f39ad938-8c42-4069-be37-3e3f975c90eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 130,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8514cc5d-ea55-4865-8b41-4a09ef25409a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "188846ec-29fb-480a-ae8a-3ef9f491bdaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 109,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e3088fe5-b746-4027-b163-34ff7c74632a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 97,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f00435de-f484-4a25-84ef-8ec5f7e1760a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 85,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ede8feed-5468-44d6-b036-fe36643512b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 73,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3f6e6deb-4137-4d76-b259-ed5eb9492c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dfb8fe7e-6db1-47ad-ac16-516f58b8bd01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7bec5046-3ef2-43ae-a02b-04bba53afe3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d86b7a77-25f3-4bb9-affe-f813890fb57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9b7ea8f5-5b09-46f2-86d8-e1404b9da7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 14,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "25ea1e49-3611-495e-bd49-a53740505773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dce3cdd1-0251-44cc-aa4c-4e57fb1612cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2e3f4939-6b6d-4462-ab35-dfdd8eae013a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d90ba249-d4e4-4112-8ae9-853cd4f33e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "910d7c0e-2229-47ad-b2c6-c608c931eb17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "97f23a3b-ba95-49ff-9faa-cf26b58ccfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "07f7ffea-af6a-463f-9f59-6bc3f5de351b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "253bd510-6d1c-4362-8341-12be40991d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0c41ad83-244a-4960-8e71-ae77c03190c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2a95d397-9531-43f5-9030-4c3df706a0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "91ad9857-8df7-4fd7-8bed-ce2ccff8e6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7f84df81-38ad-4766-b91e-f29624f2c6c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "86190dd8-72e9-411d-8e96-678978ae3292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fe238a40-ba17-4534-bb1e-ce76d85bb41f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "eab8ab63-0a57-4fd5-862a-823575bfb68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cefba07f-263f-4216-8653-b0f1eab97b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cc52cb60-f8de-4d14-988e-d265693dc9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2bfff85f-73ca-454f-a547-da3baac12d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "998afe98-0882-4aed-b50c-579b8bc95b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3835019b-2a07-4d2f-917e-dafe95256699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2c1eb115-07ff-4d31-afcc-563fe4d5b6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f46410fb-f7ff-4d2f-b7f9-db8a7253b7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "90baa774-02ec-45f2-ac07-15003cdbf90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ce90176a-4f50-48a7-9663-479bc723e7c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6ea28f67-a279-43ba-85f5-21b45286fb05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "369a9adf-e55b-4531-af4b-58a8df82e908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 115,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1eff65db-bb5a-4506-b385-162e2bcd8049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 13,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c478551d-6fd5-4851-96d5-1e10aea40856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 224,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2f5504a0-847d-4beb-865f-ff0066b0eefb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 213,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d8ccf098-1450-4a4e-a38d-ba04cfe642c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 207,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ddde8e8d-94ce-41c5-ab7e-3a5528cfc0b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 193,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b704f263-3392-4962-8c9e-9597934dfd80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 181,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "846b4c2a-39f8-4de5-98c9-102e3f529940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7cecbe21-2f60-41c5-ab0f-1cd16fec6269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "da98b49f-1fc2-47e4-b323-3e72634f5573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fcbad1be-cec9-4aec-9af6-b76702b5c495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 136,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8addd992-1fda-4a5d-b557-a3976129dfb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 232,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2dbdd4fb-fddf-4602-9edb-daade2b4d3f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 127,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "006232d7-9eda-4ec5-a9e1-1d19f6d2b42b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 104,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8d88e41e-dd83-4319-912c-54d53d0e1490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 92,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d2312809-4057-47d5-b605-357218efbfca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 78,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "35389cd2-f8a3-43ab-8af1-ee76fbec11e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 67,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a618096a-fbd2-494a-b2b1-1f4b8fde0dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 56,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b3f56d84-917e-400b-9951-a56183e72662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 45,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7460ca98-13ba-4dbb-ac64-1655e1b3f263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 35,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f8fbf3b6-259c-493b-aeba-3a17890b1c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "989c00cd-972b-4817-b248-176e57ed3120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 20,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7d431126-dc0e-484e-83f8-79707da798c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 13,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8e75facd-321e-4d9e-9b5c-54970e0905ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 25,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f18fedae-bab4-44a0-9639-632a20087839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "43374c08-a178-4299-aceb-2484854429d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 65
        },
        {
            "id": "591911ea-4c9a-478d-ba96-55ce0dcee907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 66
        },
        {
            "id": "03c39d65-3348-4312-a416-4f90808b5849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 67
        },
        {
            "id": "dc35e7af-f803-4d42-acae-7a097e5e74b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 68
        },
        {
            "id": "b2d9efaf-f08c-49ca-b96f-2d04185d0864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 69
        },
        {
            "id": "25c2ba5e-319f-4364-88c9-22f34a1f3802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 70
        },
        {
            "id": "d41fc930-e243-4cdf-8f77-2a0358f26bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 71
        },
        {
            "id": "9e4785e5-c7ea-44ef-883b-0bca5759286d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 72
        },
        {
            "id": "f48e972e-ee3e-4aa3-a1b5-876954b3a429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 73
        },
        {
            "id": "b078bd61-c1c8-4e2d-8718-734a7e7441b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 74
        },
        {
            "id": "f9d5e5a8-ae0f-494e-86fd-619e0bef247b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 75
        },
        {
            "id": "993a72db-5d95-4b35-85b1-5b3b7740478b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 76
        },
        {
            "id": "f9398c79-4e48-4704-a62d-5315ebe75d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 77
        },
        {
            "id": "be5e81d7-c8ad-4fe1-a9f2-80749297d73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 78
        },
        {
            "id": "e2a6f64a-bac2-4a0f-9f7a-274a0b4589ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 79
        },
        {
            "id": "4f65cc44-5cb8-4a63-a36a-a44ffafd0bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 80
        },
        {
            "id": "5898fb5f-7bec-457b-85d9-4430f83658ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 81
        },
        {
            "id": "e6353d1d-dd16-485f-9737-9bb39116c3d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 82
        },
        {
            "id": "f7dd5f64-9eb3-451f-bd8b-d1b805cdb57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "355a62ec-c3cf-4902-89e7-1260c41951b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "6fdf2fef-1096-4e12-8c1a-3d0c4923876b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "a2f44a80-7144-40b1-a3fe-3dba10f41086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "c006c9b4-0ef8-44f4-852d-aed66afa15ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 89
        },
        {
            "id": "77adc7f3-7d88-4840-942b-cfd94fe9f896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 50
        },
        {
            "id": "3cae6bd6-e4f4-4b99-8a58-ebce7c9784f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 52
        },
        {
            "id": "5ad00994-7efd-41de-af2d-c27dc04ca9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 55
        },
        {
            "id": "8bc592e6-df3b-4f71-8021-68ff05ca16dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 57
        },
        {
            "id": "e163d132-3f59-4a92-baf5-96d7ee1b60d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 52
        },
        {
            "id": "7a965432-7034-4e27-bdc9-8d041551f0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 55
        },
        {
            "id": "128eef99-e8d9-4a79-9a3f-f1caa0ee15a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 57
        },
        {
            "id": "aa9ad549-673f-4742-9291-1e86b19199c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 50
        },
        {
            "id": "7dd5bccb-7079-4de7-9966-1844a6171360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 52
        },
        {
            "id": "edebb272-999d-4977-bf13-6322d613eab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 55
        },
        {
            "id": "dd370741-f05b-40ff-a7c1-653888579660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 57
        },
        {
            "id": "e3d5c76d-984c-4e39-b9d6-f5b4508dae13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 49
        },
        {
            "id": "20418320-96d2-4237-b4d4-520f31cd907e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 50
        },
        {
            "id": "c7b5cbd1-29c6-4e0c-95f4-0adb465b482e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 52
        },
        {
            "id": "e1a4b70d-87a0-405d-81fc-35dcd74cc01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 55
        },
        {
            "id": "80589359-cd64-4b2c-9ea3-1b7912a5230e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 57
        },
        {
            "id": "7f9662a2-ae75-455d-bd2f-9cc58ba4b4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 50
        },
        {
            "id": "07b2647f-fbe6-4477-9316-34b7cf239335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 52
        },
        {
            "id": "ab44d8f6-1636-4b46-9cac-99400d98e1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 55
        },
        {
            "id": "19505fd5-4270-46b7-9d14-c7974b21912c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 50
        },
        {
            "id": "daa4a727-2af8-4642-8eaa-df527cc7d517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 52
        },
        {
            "id": "3368de6b-4439-4e49-8694-aa19369949d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 55
        },
        {
            "id": "d16d4a6d-b6e5-4e30-8e7f-ebeb7017f59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "efdb6a91-0343-427e-93d7-d9fe02aea7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 68
        },
        {
            "id": "5cfdd202-6dc2-4a8e-8fe7-1ebf9a90fed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 70
        },
        {
            "id": "c44d0319-b585-476b-b185-b99669834c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "757d2146-ad7e-4692-aea3-52256eb911b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 72
        },
        {
            "id": "fd60ee58-48e7-466b-b2ad-64def27a5a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 73
        },
        {
            "id": "a211364c-6a66-496a-9ef6-b1fc67943fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "e0d8ca08-50cc-4082-94d8-38a3c13218fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 77
        },
        {
            "id": "0321f6c4-cc37-4793-b483-2bb88a195e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 78
        },
        {
            "id": "ec988153-be56-4c9b-89a2-0b6d0c7418b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "92c7a940-2ecf-4ae5-9fab-502f4096ddfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 80
        },
        {
            "id": "f189a12a-17bd-4ab2-85d2-4afdcedcb106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "e1461b95-383f-4cb5-a873-33bbdec158f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "3433de72-5a1e-4cd7-bdd0-65aa042b4133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "9821d4fd-862a-4e46-bfcf-020af130fa84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "09aaca41-c722-4c51-995e-6b4ae7f9ef0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 39
        },
        {
            "id": "1bb18015-49ba-4238-990b-25a2ad99f020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 68
        },
        {
            "id": "1f68c4dc-dab0-4053-b05d-21b8bcf08686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 72
        },
        {
            "id": "0bc9bb42-65f2-4b00-bc07-3c6c4ab24ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 73
        },
        {
            "id": "66a7be1e-a871-4f22-9d61-66f9209ef4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "9b818b84-fd01-4533-b824-46988aac50de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "fd7e3676-651f-4ecf-aa0f-18ae5020dd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "0f8b7864-1c66-473e-9bb2-cd3a4626ca78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "2e546c12-4c94-46e0-861b-d28bf2b02489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 39
        },
        {
            "id": "a9de3f95-98a5-46e0-beed-1875f2334fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 65
        },
        {
            "id": "39bd985f-487e-4e90-93b8-aa5f4a4687f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 66
        },
        {
            "id": "3900004f-6e19-42dd-a168-44dc28530aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 68
        },
        {
            "id": "07e85e89-2a42-4931-aa64-9236c519ad08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 69
        },
        {
            "id": "529f6348-45f1-412d-a648-365e39a8eff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 70
        },
        {
            "id": "44bfa0f4-142b-4f81-92fd-63242557a425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "fb19c3df-a2b7-438c-8b9f-39b040cdef9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 72
        },
        {
            "id": "52aa7014-a8a7-4bdf-9b05-f2e893061a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 73
        },
        {
            "id": "54aa9d6f-ffad-4dff-acab-14797a589359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "2a532382-62e4-49ff-acd3-c2eca6017f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 75
        },
        {
            "id": "dbf821cc-75a2-446f-ac69-798aef0ba177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 76
        },
        {
            "id": "85a45a88-4a62-4f74-aac7-3153ce2b7a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 77
        },
        {
            "id": "3f9285c9-b6b2-4327-9fce-151e6e208263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 78
        },
        {
            "id": "4870a50c-a026-423b-9dfb-dce591cd8d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "5faf16cc-f660-4d5b-b5f7-ff502f2d2f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 80
        },
        {
            "id": "6d789d3f-780b-44bd-9719-6708423cc2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "1042076a-6a9c-483c-82b2-752059d787f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 85
        },
        {
            "id": "edd0e2a0-2dec-46c5-a080-5414b7747727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "10adcaa9-9910-4348-ab7a-41e6bfe3b234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "6d7f16a7-f426-421f-a031-6ac7e82b955f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 89
        },
        {
            "id": "fe86f7e1-2cf0-4ee2-9afc-3e2a77d0990d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "6d872c7b-5b07-4968-9216-881e7eaa11c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 73
        },
        {
            "id": "f8fad1ed-02ed-42e8-bce4-a88b30d99a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "8fd62293-609c-427d-9a07-a89891354fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "ef7bc571-c48e-4cf6-bffb-1d2bdb5c5c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "95ece795-ec6c-4a97-baba-c6298011415d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "93fde2bd-fca6-4036-af09-d58757a24bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "299eb6c0-b579-4c11-a8a7-e15f9d3bfc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 39
        },
        {
            "id": "099e1587-a61a-427a-b8c8-b5ba088e8555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 67
        },
        {
            "id": "66041eb2-5969-4402-8e81-5ef5aea96bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 68
        },
        {
            "id": "bb681e54-e014-42ae-b3d5-e990d3783662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 69
        },
        {
            "id": "53a3013f-7929-4e56-9831-9b1d6f295713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 70
        },
        {
            "id": "c9cbe9bd-8ed0-4050-9259-192811277e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 71
        },
        {
            "id": "8de0ca79-8b9c-4f47-8024-21cd25b2d3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 72
        },
        {
            "id": "17a77b5d-ca04-4453-90db-07eaea95c3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 73
        },
        {
            "id": "a401979f-e087-4693-b96f-67bd3ea95c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "a6cc2a33-261b-4763-8056-8987559823f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 78
        },
        {
            "id": "5fcd0362-3d3f-4026-a242-f09730c762fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 79
        },
        {
            "id": "d0eb9970-c06a-453a-8353-d08c7cc63c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 80
        },
        {
            "id": "04a140c5-8937-41ec-bb8e-10afb9e04296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 81
        },
        {
            "id": "d266f73c-728b-4e6a-bfa3-c22b52730a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 82
        },
        {
            "id": "5382ddcd-f355-4720-8657-92121850a25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "2bdba242-3334-479d-9e91-9c3ffee2146a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 85
        },
        {
            "id": "80eebe51-a290-4174-a22f-3be9e39aa542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 86
        },
        {
            "id": "6eb26e7c-a9fa-4095-befe-860855df1432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "cb95812f-7398-44e0-8d0f-fb46e0dcc137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 89
        },
        {
            "id": "4a0af414-e721-440f-9c20-8c7cc56353f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "4402e84a-ef99-4941-b700-32b98812ecf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "744860d6-60cd-48ac-b903-41b84ebac14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 71
        },
        {
            "id": "52179ec0-03bf-464b-ad27-9dc82bd418d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "ff48f30c-864c-42a2-be34-d909a49fa93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "6125359b-d0fc-4cbf-8fa9-01eb861379fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 95
        },
        {
            "id": "9b2d2fcd-cbf9-49d7-b747-70336a9ed302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "ff50e641-deda-4143-9bbb-0a50ab3db852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "f1ecf0f3-1ba8-422f-92db-638315efcd5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 39
        },
        {
            "id": "8e2a4a9a-3754-4101-89f0-02ac6e61d57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 73
        },
        {
            "id": "8b40ba40-a7a4-48dd-96df-946abcdffaee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "044865d2-a22d-433f-80ba-8c6f75d1fb58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 86
        },
        {
            "id": "4ffeb7d0-4980-48f3-b6aa-d9d9693faeb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 89
        },
        {
            "id": "b0b1629a-b3cd-475f-b517-d6d56f779c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 39
        },
        {
            "id": "a69daf38-0199-4b91-929d-e9d38569bdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 55
        },
        {
            "id": "bf4b6faa-06a5-4e85-a278-68d2c1738805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 63
        },
        {
            "id": "ae225720-2982-4db3-8928-b76f06ee9245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 65
        },
        {
            "id": "4e3644a6-45ac-4f0a-9225-07807c6084af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 66
        },
        {
            "id": "41aa4c08-45c3-4d88-9316-93e4721bcb86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 67
        },
        {
            "id": "253a6977-ddd6-4fbe-8854-0172773e3f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 68
        },
        {
            "id": "055c2aa3-c8eb-4bbd-8de4-77b710367efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 69
        },
        {
            "id": "daebf839-3cc3-4cee-a0b9-cffbbaaf26c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 70
        },
        {
            "id": "e134b0db-36df-4a63-9d86-74fd8b779994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 71
        },
        {
            "id": "c937194c-314e-411d-9174-502d910d506b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 72
        },
        {
            "id": "f86be8b1-4c6b-4589-8e67-25cd19d55a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 73
        },
        {
            "id": "dec4c742-6d2f-448c-bf7d-dba83b3d6e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "34ce7a67-abb8-417c-9612-6d92a5979d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 75
        },
        {
            "id": "d604fce6-1f50-484a-97fd-3e55a12cdd41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 76
        },
        {
            "id": "c59806dc-1739-4e9f-bb27-bd9c66dd6e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 77
        },
        {
            "id": "1bcd9c0f-44b7-42c1-9f0b-7da513d3e014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 78
        },
        {
            "id": "db65ea31-ea2e-436b-9ee9-d61805f7118f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 79
        },
        {
            "id": "0cb28f0a-24be-4377-aadb-2a3e82ac5489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 80
        },
        {
            "id": "f1a45274-354a-4c2b-89cf-c81e172aad7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 81
        },
        {
            "id": "82bdd764-df50-4a97-99b6-55febd373fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 82
        },
        {
            "id": "afdfc1b7-0c50-4ea9-a57b-0213823cb9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 84
        },
        {
            "id": "46f068ab-7ce7-4b81-8102-711f5b13a88a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 85
        },
        {
            "id": "68d9215e-30f0-4e91-8cb7-ed4e34f8274d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 86
        },
        {
            "id": "8ad021d9-0894-4152-afd3-76272612c53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "b4482336-a07e-4b92-8fc5-fd97f961ce59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 89
        },
        {
            "id": "33d66581-f350-4346-b250-ff46c5e203ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 39
        },
        {
            "id": "f2652a88-0439-478c-97ec-6a1bb7047657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 73
        },
        {
            "id": "48b58088-8f22-4e81-81c4-decaad65c2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "86909db6-d755-4225-abd3-5c0145f27bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 89
        },
        {
            "id": "e91c8f12-bb8b-4fe1-8ef7-d65909d97309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 39
        },
        {
            "id": "5729e892-de1d-423a-92ef-1cd910d6c4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 66
        },
        {
            "id": "1d0f2434-3bd4-4a1e-b920-fa2f3ac99793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "3e6872b0-be65-4291-8ba8-1643f24e4ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 68
        },
        {
            "id": "82b54d19-84ee-492b-8696-5ac3d0b180a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 69
        },
        {
            "id": "f3730dec-2cf1-45f1-8d50-4b6b86f43583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 72
        },
        {
            "id": "c27a110d-95e8-44f5-85d8-e2724f9bd204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 73
        },
        {
            "id": "2eb82247-3890-4bdb-a3a2-4c18d2c5f0ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "44b175be-ee6d-4958-8ee6-5ee28cae3167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 76
        },
        {
            "id": "d6967c7f-e7a4-44e2-a76e-4409afea798d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 77
        },
        {
            "id": "f6904ce0-1f80-4d3e-8c14-8f574796e0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 78
        },
        {
            "id": "2d352ccf-31d4-461a-94d1-bba30f919639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 84
        },
        {
            "id": "c4e7ee80-2c01-450f-b270-42d485b7a6e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "61b4887d-fe4b-4fdf-9c4a-4f1ff96c1f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "7d233999-95c8-492c-8380-ac345c897bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 52
        },
        {
            "id": "1330202b-5e94-4480-b51c-de3a6ab5fa14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 55
        },
        {
            "id": "b534f211-8a0d-4a0f-abc5-7d24aa1325fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "b242c405-87f2-4052-aaa3-d7d782271f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 63
        },
        {
            "id": "5033b49f-e522-45e9-92d7-cac88dd1d3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 65
        },
        {
            "id": "f3b48ea5-7618-4a3b-aa0a-28632f313842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 66
        },
        {
            "id": "d51f04ea-2892-4ead-9085-0c39b733474a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 68
        },
        {
            "id": "36651777-199e-4247-a4e1-2c80a884fa76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 69
        },
        {
            "id": "acde7206-7fb8-46f4-835d-888f54ce82ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 70
        },
        {
            "id": "b9609718-b7cc-4306-aa28-a44dde7b9d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "542be7ef-c4ed-4fe3-9c13-aa58424464c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 72
        },
        {
            "id": "6a596a94-8d04-4e92-98e6-7ff97059ca17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 73
        },
        {
            "id": "5782227a-5455-499f-80b9-a8f01c9b9f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "fb492bd2-89a3-4107-93d9-56021a052645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 75
        },
        {
            "id": "daed5974-b3a2-4be6-9e3e-5112aab2c57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 76
        },
        {
            "id": "2688e383-d41a-4256-8ca1-513ccfc76903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 77
        },
        {
            "id": "d583bcfa-ea10-4ba7-a404-cfd739340943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 78
        },
        {
            "id": "d0590031-c89a-4fda-a9a6-56cc5de3736a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 80
        },
        {
            "id": "2131cb4d-562e-4466-9b74-1e027f18d98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "2beea82c-41dc-4f85-8439-03823bc6dbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 82
        },
        {
            "id": "475c2a16-6b2d-4a49-adfe-0b7dee556d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "cfe4fa1c-3f88-4160-96d7-126fb967f165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "40c5e7e8-188f-4baf-928b-7b2c7d1fdc73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "2bb3c40f-ea6c-474b-8745-bf3fa8dfac5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "712efa2f-cddb-4219-85e1-202029046fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "b984782a-1f93-4ad9-ab7f-9c4f472be74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 44
        },
        {
            "id": "21abb729-ee87-4e6f-93f9-262355f17b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 95
        },
        {
            "id": "15f0c946-96e7-461a-9d37-711ab4c01d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 39
        },
        {
            "id": "93d14a77-c38c-4421-9c62-93460248989a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 72
        },
        {
            "id": "3db82056-94b5-46ca-82ef-9cb9e90dafa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 73
        },
        {
            "id": "626a5cc8-c4fe-426f-b8e7-f2718bc208e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 75
        },
        {
            "id": "bbcf177d-324b-4eb1-8171-0c2d04462eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "affc42e7-0571-4acb-991f-7ece2d8151e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 85
        },
        {
            "id": "ce9c88ae-6123-4dd4-af4f-9eb8db99ea8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 86
        },
        {
            "id": "f882a23e-11a8-4df2-85c4-5ee38c3377ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 89
        },
        {
            "id": "b5c3a47f-cfbc-4ac2-b37b-21d07bd576e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "66188d3d-fb81-451e-bba9-9a788f41ffd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "36be9c49-6f71-4fa2-90bd-420434259edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "b73a5ba6-5045-4eb5-be2e-071826e3ad34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "07dff744-a073-425f-9472-84747940601a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 95
        },
        {
            "id": "2e5832c4-a6c3-49c0-8d75-624da9c3ac82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 39
        },
        {
            "id": "40b77c9b-b629-435f-8f27-d5819bb18464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 72
        },
        {
            "id": "932ee4ef-217d-4e9d-8c1b-8b50b1512765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 73
        },
        {
            "id": "6d1a5ea2-9198-453d-b578-b972ecc12bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 84
        },
        {
            "id": "e6d4860f-2cb9-4083-a391-e1035670e32f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "e3c041d3-c06f-4eec-93dc-c825f82f6baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "f3aed815-6445-4e74-b115-f5e818bce24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "c7d6f2cf-ad1f-471a-901f-9e6df9a62986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "f7d6c62a-f591-4f3e-b830-d49f46f1e572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 95
        },
        {
            "id": "885d6197-cf31-4a97-8119-078a9e806e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "4995fe6c-8467-4f06-a704-a41c30e56da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "2f87a514-6ba3-4b71-9862-6f508ee124fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "f48ecc5d-3218-467a-b468-b9f8faf936d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "5b9409e4-6ab7-42c5-bf46-38d88c26fb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "40661e5b-04c1-4dcc-98dd-8d190fe97171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "d98dbbc7-4817-427b-9838-6a5ab15f11f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "e42d4353-7774-479b-ad3a-9555e4b4313e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "205c0607-a903-4122-8eed-29db19ef91ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "6227221b-a8e7-4263-8dc6-0de2b4c0c133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "f160ed85-ad24-49ef-8e83-f9d2e0db9f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "b607d4e2-e171-4d54-8826-99ee650a29b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "a5bf8cde-fd33-4bf3-8739-60d211140e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "47d6678c-ebf3-4483-b484-773855a47c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "11c937c4-3f4e-4273-8b29-940f3268c399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "26b44e34-4b76-433a-a264-66bc1c6f2302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "a320d9ad-f8b2-4157-ab2f-e4483c81683a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "dcb5d6e0-71e5-4f6a-a7e3-42ae97004ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "dd4485b5-b8c1-4178-b1d9-027931bb167d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "75c6d79b-8a31-4cf4-a0c1-31cc87779907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 39
        },
        {
            "id": "db953e6f-8c4f-47ad-b3e2-a3f1c00c5a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 73
        },
        {
            "id": "30248dd6-3a70-45e4-ae41-b524aa3ac734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 84
        },
        {
            "id": "eaada91f-abad-4714-9e78-5152a67db960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "b1b05a4e-81bc-4774-9354-7ce685160664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "d620c890-dc3f-4360-a532-860c995d28af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "b24501bb-3d3b-4096-a012-556dbbd6f45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "95b27cff-c3e7-40b4-baab-b730dc0b2745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "bb951645-3724-49b9-a1b8-ad40fc968352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 95
        },
        {
            "id": "64f39cae-2642-4f26-b76f-85b3aa5d6184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "e535e70d-2e0e-441e-9132-b943934c2870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "3be7fb3d-f99a-4942-8a86-7487a1a16ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "330ec3f6-3127-4638-9c08-ea4f70f1109b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "a4a11a70-7835-4c7b-8e7e-ebbe60da8234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "beab49e9-4bfb-46db-908c-929a57694eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "82b6c9aa-256b-4621-abc7-596fb2a5a979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 95
        },
        {
            "id": "43bd245a-aa91-4e61-9cc3-0fd082992069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "4154a8d1-bb49-4763-8132-4802ebd8bebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "b9f70cad-f381-45fd-a495-541a8267dddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "08e5a5ef-63db-4f9d-bb1b-68826a3208a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "176f91f6-402b-4c50-a2dd-23e7debdc4b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "a5103e26-7cff-4662-8cc1-fff8ecba2e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "d55c5154-1195-4703-ac06-dbaa371a0db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "1d08942f-9a58-4b6a-94cf-fee3499f7dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "78704575-91b7-4631-a0d5-ae97d38ae19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "371f2c2a-a9a5-4a88-8091-736261a66a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "57e181fe-9ddf-4122-a33f-c1fdefbb561c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "cd865f7a-4a47-42fe-b0ef-aba93a707663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "6e76ec5e-41b3-45ea-a359-b4591246ebb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 65
        },
        {
            "id": "94f241cc-bfc1-4f54-b061-b3b57b58f0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 66
        },
        {
            "id": "890f78cb-5e68-41c3-a9a7-5cb8cc295ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 67
        },
        {
            "id": "34e2a5ab-a1bc-46c2-a4bc-c2317eab88f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 68
        },
        {
            "id": "3666bc8b-e5aa-48a5-9bae-f2b4d5cbec78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 69
        },
        {
            "id": "6154c947-b5c2-4c08-9ce2-b6d442eb0177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 70
        },
        {
            "id": "d99bf7e3-92ea-42df-abb8-8f20c0a91032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 71
        },
        {
            "id": "d26426a2-1b98-437e-a3cc-190ffed5a897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 72
        },
        {
            "id": "82227539-08a3-498e-b643-22a776e3ca7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 73
        },
        {
            "id": "6e9b6735-0ce3-4e19-8484-3acd14108d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 74
        },
        {
            "id": "e0f70e5f-fa0a-4118-ad89-f14e1780e252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 75
        },
        {
            "id": "bcac11e1-0fcb-405a-a0fe-c1cde354d953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 76
        },
        {
            "id": "1f20a721-d647-4272-ba5f-49b8053b74a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 77
        },
        {
            "id": "e9d6425a-361d-47cb-8684-739688f9c7f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 78
        },
        {
            "id": "c976e36e-5b4f-403d-880d-79b3ded777cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 79
        },
        {
            "id": "af4b3bc3-d5ed-4b43-9ffc-6608572a64b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 80
        },
        {
            "id": "a76e3fde-48f7-4bc5-9686-3107cf903148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 81
        },
        {
            "id": "0bb687bb-1e51-4ddd-a8ca-07aeed32cb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 82
        },
        {
            "id": "c055d527-96ba-4423-9a12-0894dba0eb27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 84
        },
        {
            "id": "bd9c275b-7815-4133-8fbf-e8c69f21b39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 85
        },
        {
            "id": "afcf3ac7-8335-4556-b234-7913e7a3a8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 86
        },
        {
            "id": "41da3427-26d5-44e1-ab21-fac302feba66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 95,
            "second": 87
        },
        {
            "id": "ffb9416f-d7a5-4a90-b706-ed9c0462801f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 95,
            "second": 89
        },
        {
            "id": "62d38646-34e2-4e98-a37a-6dc826a5f3b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "c36a0aa3-5575-4e87-b108-6bd9b05c10c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 55
        },
        {
            "id": "39a3836e-bf71-4efd-91ff-24944e2d4a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 63
        },
        {
            "id": "39731007-7084-4205-bb29-8e96b2feaed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 84
        },
        {
            "id": "4cfc2dbb-c883-4e26-8a79-0248119f843c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 89
        },
        {
            "id": "9cee11c8-baef-4057-bdd1-bbcf37880106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 98
        },
        {
            "id": "9669a01d-1643-4051-a52e-d5cebacf8264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "ced43a3f-c9f1-41f7-8a11-84446af96816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 108
        },
        {
            "id": "83097c7c-1234-4b63-998d-10363db6e708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "fd1745a9-ed17-440b-b498-6d67bd669f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "24404499-bd5d-405e-87bd-1c7754692e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "866826f5-bc4d-4f68-b281-130d4a46d58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "683afef1-022a-421a-b63e-1eafb5a10971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 89
        },
        {
            "id": "c9dac64b-d919-44bf-a508-76affbb3ec93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "5db2a759-77cd-404c-9bca-c663a124b918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 39
        },
        {
            "id": "09cb24b4-a341-4ebe-aa6f-fded7ddf3fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "93ddbfa5-179b-4da8-b89a-73962d99bb7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "af6d488e-8c14-43f4-b7a1-c7e32a70fb33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "34552593-4feb-4b58-ac94-4b44369df777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "3295afcf-4b7d-40b1-bba3-d81d1aaaf812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "e4024309-b7eb-4b0a-aa48-0cbb369e9aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "5051a4d9-a892-4596-9883-96f4e383fed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "1f4244a1-8bdb-431a-98cc-36264a1f26d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 84
        },
        {
            "id": "55790859-5496-4ed4-9cb0-0decc915d581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 89
        },
        {
            "id": "1d71d592-47df-48e0-919e-09ef2f6e3e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "32d0eeee-3338-4fce-867a-f082282870c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "e0b4999d-d1d1-46c8-a051-99a46da31874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 99
        },
        {
            "id": "70ba261f-1802-4ab5-b383-7fc381e3f2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "65864a8e-0e9a-404b-ba6c-fdb5bd6cb119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 101
        },
        {
            "id": "c8374961-2942-483d-8a37-05a8900adfe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "dc050010-c127-4c0e-9676-ab21202c4115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "317fb365-80d8-41e5-8c32-4f2eeb6fbeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 106
        },
        {
            "id": "8db46650-ae36-4534-9801-5d12e0f871fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 109
        },
        {
            "id": "62f38310-9007-4690-b75c-9c502b9466ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 110
        },
        {
            "id": "6b869819-5a5a-4827-b6e1-88f1f71f13ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 111
        },
        {
            "id": "beae77e4-65f7-4dbe-8237-483d0646fe5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 112
        },
        {
            "id": "c4520f8e-dac0-4d7e-bf64-6b28b858e3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "f0eafebf-fac6-4721-a776-567c2f25edeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 114
        },
        {
            "id": "ac2853b5-0a74-4a13-9488-1d14826dc61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 115
        },
        {
            "id": "b01b2ee1-fd36-4374-bfab-d9b5cd9957ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 117
        },
        {
            "id": "80e05fdd-6ba0-44e2-8ac3-7d3ae50f7224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 118
        },
        {
            "id": "b12c29e7-99fa-43d8-86d6-8b059b6a447a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "b2901fe2-b78c-4b22-9a8e-fb710f10c2e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 120
        },
        {
            "id": "b86a5ceb-992e-40bf-b6e2-8828330df1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 121
        },
        {
            "id": "11aa4fc6-f340-4084-8c4e-89484324687c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 122
        },
        {
            "id": "5217280a-7342-40ea-a32c-442f50f7e4d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 89
        },
        {
            "id": "0998b8a6-cfae-4cd3-9242-7592f1a4611c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "02d1cdea-c5ea-4e64-91aa-79fccf1e879d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "cd22df0c-b611-4448-ad1c-a989d12974c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 84
        },
        {
            "id": "cdde7b7a-70ba-4e73-9733-275fb50cba4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 89
        },
        {
            "id": "fb893b2a-cbeb-427a-8530-3f46713b62eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "33fb7a63-b32d-4a38-b690-efabf37c8db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 39
        },
        {
            "id": "d68cda18-7330-4794-a7f4-e104aa7973a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "38c380a7-febb-4037-b4b0-edac3071d3f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 63
        },
        {
            "id": "84bc16c2-78e2-4b53-b852-9aad40e7012b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 84
        },
        {
            "id": "e0dc4eea-e300-46f0-8808-bc25a7502d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 89
        },
        {
            "id": "3b47a70e-fe11-4d97-985e-0f109e832e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 98
        },
        {
            "id": "d80510cc-f22b-470d-ae50-d53a49b884c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "5dedd94c-756f-4e01-bae9-e7bba4b00906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 107
        },
        {
            "id": "3689735a-b94e-4a95-92e3-39c147045d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 111
        },
        {
            "id": "32ff70ed-7881-4828-a7bc-d5966f45ce2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 117
        },
        {
            "id": "9d2f54d3-b49e-4214-93ae-5529f6dfb704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 118
        },
        {
            "id": "1ff36db8-87db-47c4-aa09-4ecd5161a01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "33e238cf-0f72-4410-944e-4c9ac3b59d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "4b7c163b-e31d-420f-b1c0-4e013b72f4e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "1d3d5c40-b8ff-44b3-8c1e-e75c36f73241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 84
        },
        {
            "id": "3fbcace1-c2a3-4802-b99c-35cc88cc98ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 89
        },
        {
            "id": "4b6bfa69-e3b2-4fe3-9979-d1e2f7eaa775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 118
        },
        {
            "id": "27aa4135-f537-42f0-b790-217c28aa2c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "10713505-9b63-4ee3-b360-232aad76501b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "0dd625ce-16a2-4702-9af6-fade99b71adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 63
        },
        {
            "id": "a6fbc9fe-f249-4627-a964-8d2f31e1f1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 84
        },
        {
            "id": "d423b8c4-0e2e-4d72-9a1b-3a02abe0b41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 89
        },
        {
            "id": "d988e9e3-d5bb-45a6-8d2b-d2aeb8fca81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 98
        },
        {
            "id": "156900d6-9dc3-48d1-b9a0-01ee31fdba7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "ad58f968-9656-48e8-b40c-0515b46597a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "38699e20-ee55-4d7d-9126-245a2c835cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 102
        },
        {
            "id": "71572b65-316d-4c70-94a5-03d3b69538ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "8c28f1f0-3fff-4e27-8393-1ff0f00eb014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 108
        },
        {
            "id": "1f11567b-24ed-402c-b860-46f98b87894e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 109
        },
        {
            "id": "202d4c8a-9d4a-40bc-b9d8-1b19dbb0c2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 110
        },
        {
            "id": "c4a35846-7b4c-41c0-a66d-12f76e8e0e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "582db79d-098a-44ed-9d1b-df1a91daaf64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 112
        },
        {
            "id": "29dd6963-9724-4aad-a226-f39584177499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "143a63c5-1820-416f-a26f-480aae00dcd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 114
        },
        {
            "id": "d66835b4-8858-43e1-81aa-c35d48a37209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "e803a6be-a4f5-4cba-ac4e-bd354280cf81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 117
        },
        {
            "id": "bb5c6801-9296-4e10-839f-ab85d42513c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "187fe38b-7410-4f64-bfac-dac5db8b3288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "87aaa1a7-90c9-4335-8f7d-83fc1e63f5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 39
        },
        {
            "id": "2150cd2e-9cde-4b21-bad4-5a25b7c566a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "6a5d43dc-4dca-43c4-aac6-6b5277a610c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 84
        },
        {
            "id": "9ff68ecb-0f35-4e40-ae6a-1a1c45406074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 89
        },
        {
            "id": "cf4032c0-1ec8-448a-81fa-5016b31bfcef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "02f82896-a9f8-4b53-9083-ebab42cb206b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 39
        },
        {
            "id": "37a16c95-2fb0-4b7b-b541-a671cc1ac810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "e706fdfa-e65e-4b60-baa0-7d33f6fa6433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 84
        },
        {
            "id": "191d7870-ee17-4c92-ad8e-becc8384e20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 89
        },
        {
            "id": "26bc72e4-d6e7-4c23-a771-bb317dbbafec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "2404b78c-8643-4764-92cf-41816cadd11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 39
        },
        {
            "id": "fda8d6c8-634d-452a-b4ed-d3de17b8d6d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "ca1a1aba-100b-4c41-8b75-673616e8ce1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "e3420fa0-ccae-4a19-82e3-202d62c0361d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 84
        },
        {
            "id": "515fa8e3-5c16-49e8-94d3-b3adbc298aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 89
        },
        {
            "id": "6e2d803d-34f4-4471-a4c3-cb013dfbe055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "f4340959-dbbe-494f-bd03-feeebcd5c7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 39
        },
        {
            "id": "be4f3fb2-d5ef-425c-889b-c19fb9b1be7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "9fb928b2-f9d6-4b97-8721-30f50ea9b1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 63
        },
        {
            "id": "3a483ae5-468a-4e4d-a171-7f53bf0c212f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 84
        },
        {
            "id": "b52a424d-9a37-464c-b14d-3bfea5e63dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 89
        },
        {
            "id": "f87241e5-4de5-46ae-82d0-53a598b9d039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 104
        },
        {
            "id": "00fa88df-73e6-4e62-b4b0-a9e0d26752fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "83f989e5-5cb2-4ec5-afd2-6580cfa2b4e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 39
        },
        {
            "id": "22c76c01-ce68-4481-b272-01920c51353a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "284de13b-e6a2-454f-ba7b-3ed8ac95e7a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "fc59dc11-8d8b-48ab-a364-281db649cdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 84
        },
        {
            "id": "f3370e9d-1ddd-4674-9745-d247815ebd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 89
        },
        {
            "id": "881a476a-05bd-4416-8ddd-26a0212cf975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "292509c6-10cb-4d55-89e3-e2dcb7547e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "6d2ce1a4-6197-4d31-b778-69c4af11362c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "191433a2-38e0-4211-9867-63de8a3406d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 39
        },
        {
            "id": "bcea22a3-e7e2-4dce-a605-62371ab7e2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "4c2b6273-b78b-431b-a3fc-840d3380344f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "5b633548-ef5c-4df2-b4aa-f135a1dbc188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 89
        },
        {
            "id": "dcec9d07-781b-44cb-a2a2-a7b272c7efbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "9e97b2ed-28ea-4a08-a310-ed97acb6b16f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 39
        },
        {
            "id": "6e870599-d96f-4ba1-8baf-11dd081421be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 55
        },
        {
            "id": "51225c6c-0e2b-42ce-8e8d-8f5488070c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "34c088a5-56f0-4ddc-a3cc-90c652af0f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 84
        },
        {
            "id": "48b64232-4c20-4888-b581-949192738385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 89
        },
        {
            "id": "0600c7cc-0ed3-47e8-a519-60736f0f8aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 98
        },
        {
            "id": "872e2a80-2a40-4cc1-be86-1c71b663e6b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 104
        },
        {
            "id": "d30fd0b8-2ccc-44c6-80c8-9246fa5ebd9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "25ebd4fd-5c4e-4b45-9d2f-d1d081184001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "c8cfcb66-aebd-40fc-a787-df9880c65516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 39
        },
        {
            "id": "7759fe4b-b0da-48cb-9125-864e761ba0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 55
        },
        {
            "id": "f6645978-b9c3-4ae8-933e-e76a6b369bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "62783beb-4eb9-4cc0-a335-ff8e4b8478b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 84
        },
        {
            "id": "cacdb1a5-cfdd-46ab-839f-04bff13cf737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 89
        },
        {
            "id": "c22f28d4-a401-464e-a34d-6335e7d3c854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 98
        },
        {
            "id": "6b73793a-931b-4206-8819-76313d773ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "79c72f3f-a5f9-40ac-b737-8bdd64e905bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 101
        },
        {
            "id": "b6a3c331-2801-48b6-a77d-2abc3c2b2b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 102
        },
        {
            "id": "b908753a-82d0-4675-942d-a5a4114b7e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 104
        },
        {
            "id": "0e5683db-552e-49fc-93f4-d3c7cf44d1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "1ef769af-4674-4abc-85c7-1e1dab7eae36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 113
        },
        {
            "id": "0b5dbb21-3135-45b5-8b40-6b4a86dded2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 116
        },
        {
            "id": "9901b295-59ea-473a-ba49-f3b528cc697f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 117
        },
        {
            "id": "97592c39-7807-4ca4-9a1c-f73fe2f173ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 118
        },
        {
            "id": "623dcfb9-a181-46cb-810c-2c3a8b882f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "3dfef5f7-3dd6-43b5-803c-dcda3ef5d119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "76f48fbb-9476-4159-84a9-90f39cdd458f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 106
        },
        {
            "id": "9c3a6289-bff0-41c0-bc6e-ea48b9a223ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 39
        },
        {
            "id": "9204f39f-a7c8-4a6b-851d-ce20a5e6abdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 63
        },
        {
            "id": "b0964295-9138-438e-aa75-59bfdab4fc58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 84
        },
        {
            "id": "0a92189d-a0a2-4e1e-8746-472960c2b0c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 89
        },
        {
            "id": "24a4fefd-0de4-43dd-9157-203c52e3743d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "eab6a549-954f-4f65-b022-37a8c63e8863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "6229d4ce-3667-4b55-90b2-18d6612582f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}